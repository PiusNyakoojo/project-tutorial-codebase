/*

Welcome to the course

Welcome to the course. I really hope you enjoy taking it.

I recommend joining the Facebook Group that goes along with
this course for support, tips and additional content.

https://www.facebook.com/groups/2300897213531429/

Just a heads up that if you are using the latest
version of Three.js the file Detector.js has been
refactored into WebGL.js
three.js/wiki/Migration-Guide

For the threejs examples change the following:


In index.html change:

<script src="../../../libs/Detector.js"></script>

to

<script src="../../../WebGL.js"></script>

In game.js update 

if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

to

if ( WEBGL.isWebGLAvailable() === false ) {
  document.body.appendChild( WEBGL.getWebGLErrorMessage() )
}

*/

/*

Client-Server Model.

Server <--> PC
       <--> Mobile
       <--> Laptop

*/

/*

node inspect app.js

debugger

*/
/*

Vertex Shader.
Fragment Shader.

*/
/*

(1) Three.js
(2) Scne
(3) Objects
(4) Lights
(5) Renederer
...............................
Mesh: Geometry, Material

Camera
...............................


*/
/*

FBX Loader class.

Multiple Objects.
Animations.
Materials.

*/
/*

3D Assets:

(Look for FBX File)

Turbosquid.com.
Bitgem3D.com
Cgtrader.com
Free3D.com
Mixamo.com

Create your own assets:

Blender.org

*/


