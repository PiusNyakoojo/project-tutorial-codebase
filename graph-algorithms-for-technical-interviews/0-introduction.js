
/*
Graph Algorithms for Technical Interviews - Full Course
By freeCodeCamp.org
https://www.youtube.com/watch?v=tWVWeAqZ0WU&t=1973s&ab_channel=freeCodeCamp.org


graph basics
*/

/*

Graph = nodes + edges

Nodes . . data values . . 

Edges . . edge or connection between nodes . . 

*/
/*

Edge is a connection between a pair of nodes.

Graph is good at describing the relationship
between things.

Nodes examples. Courses. Prerequisites.

Directed Graph
- Travel from A to C. Not to E.

A --> C
.     .
.     .
\/    \/
B     E


Undirected Graph
- No Arrows. No directionality.


Neighbor nodes.



Nodes as circles and arrows as edges...

In a program . . adjacency list . . 

Hashmap data structure

Adjacency List

{
    a: [b, c],
    b: [d],
    c: [e],
    d: [],
    e: [b],
    f: [d]
}

A has 2 neighbors b and c.

Even if a node has no neighbors. It should still
have a key in the adjacency list. That way you
can still know that the node exists.

*/

/*

depth first traversal on the graph.
breadth first traversal on the graph.

*/
/*

a, b, d // depth first traversal
c, e, b (repeated)


a, b, c // breadth first traversal

*/
/*

Explore the nodes in a graph but in a different
order

*/
/*
Breadth first explores all directions evenly
instead of going in a deep direction.


Depth first: explore one direction continuously
Before exploring the next direction.

*/

/*

Depth first traversal: Uses a Stack.
Depth First Traversal: Stack

Breadth first: uses a queue.
Breadth First Traversal: Queue

*/
/*

Stack is like a vertical data structure.

a is on the stack.
pop a off the stack.
look at a's neighbors
c is on the stack.
b is on the stack.
pop b off of the stack.
look at b's neighbors.
d is on the stack.
pop d off of the stack.
look at d's neighbors.
f is on the stack.
pop f off the stack.
f has no neighbors.
pop c from the stack.
look at c's neighbor.
e is on the stack.
pop e from the stack.
e has no neighbors.

result:

a, b, d, f, e

*/

/*

Arrow to represent the directionality of the
queue.

initial the queue with a
remove from the front of the queue
a is the current
b is added to the queue
c is added to the queue
remove from the front of the queue
b is the current
d is added to the queue
remove from the front of the queue
c is the current
e is added to the queue
remove from the front of the queue
d is the current
f is added to the queue
remove from the front of the queue
e is the current
remove from the front of the queue
f is the current

result:

a, b, c, d, e, f

*/


// Iteratively

// const depthFirstPrint = (graph, source) => {
//     const stack = [source]

//     while (stack.length > 0) {
//         const current = stack.pop()
//         console.log(current)
//         for (let neighbor of graph[current]) {
//             stack.push(neighbor)
//         }
//     }
// }

// Recursively
// const depthFirstPrint = (graph, source) => {
//     console.log(source)

//     for (let neighbor of graph[source]) {
//         depthFirstPrint(graph, neighbor)
//     }
// }

/*
No obvious Base case
*/


// Iterative Approach

// const breadthFirstPrint = (graph, source) => {
//     const queue = [ source ]
//     while (queue.length > 0) {
//         const current = queue.shift()
//         console.log(current)
//         for (let neighbor of graph[current]) {
//             queue.push(neighbor)
//         }
//     }
// }

// const graph = {
//     a: ['b', 'c'],
//     b: ['d'],
//     c: ['e'],
//     d: ['f'],
//     e: [],
//     f: []
// }


// // depthFirstPrint(graph, 'a') // abdfce

// breadthFirstPrint(graph, 'a') // abcdef


/*

has path problem

*/
/*

{
    f: [g, i],
    g: [h],
    h: [],
    i: [g, k],
    j: [i],
    k: []
}

Directed. Acyclic Graph.

A cycle is a path through nodes where
you can end up where you started.
If you did a circle on a cyclic graph,
you would get an infinite loop.

*/

/*
src: f, dst: k
*/

/*
Is there a path that exists between those
two nodes.

Depth first search. Breadth first search.
*/

/*

Depth first search and ask if the current
node is equal to the destination node?

Depth first search. Obey the direction of
the arrows. DOn't travel up stream.

Start at j and try to get to destination f.

*/

/*

Never hit the destination? Just return false.

*/

/*

You can use a stack and do it recursively.

Or you can do a breadth first search and
use a queue.

*/

/*

n = # nodes
e = # edges
Time: O(e)
Space: O(n)

n = # nodes
n^2 = # edges

worst case graph

Time: O(n^2)
Space: O(n)

*/


// Depth First Search Solution
// Recursive Solution

// const hasPath = (graph, src, dst) => {
//     if (src === dst) return true
//     for (let neighbor of graph[src]) {
//         if (hasPath(graph, neighbor, dst) === true) {
//             return true
//         }
//     }
//     return false
// }

const hasPath = (graph, src, dst) => {
    let queue = [src]

    while (queue.length > 0) {
        const current = queue.shift()
        if (current === dst) {
            return true
        }
        if (!graph[current]) {
            continue
        }
        for (let neighbor of graph[current]) {
            queue.push(neighbor)
        }
    }

    return false
}


const graph = {
    f: ['g', 'i'],
    g: ['h'],
    h: [],
    i: ['g', 'k'],
    j: ['i'],
    k: []
}


// depthFirstPrint(graph, 'a') // abdfce

console.log(hasPath(graph, 'h', 'k'))







