

/*

How to Design Retail Recommendation Engines with Neo4j
By Neo4j
https://www.youtube.com/watch?v=oMTmG4ClO5I&ab_channel=Neo4j

Recommendation Engines.

Powerful, real-time, recommendations and personalization
engines have become fundamental for creating superior
user experience and commercial success in retail.

Creating Relevance in an Ocean of
Possibilities.

How Graph Based Recommendations Transformed
the Consumer Web
- People Graph "People you may know"
- People & Products "Other people also bought"
- People & Content "You might also like"


Today Recommendation Engines are At the
Core of Digitazation in Retail
- Product Recommendations
- Promotion Recommendations
- Logistics / Delivery


Powerful recommendation engines
rely on the connections between
multiple sources of data.

Neo4j in Actions

How to Build Recommendation ENgines For 
Retail with Neo4j

What are the Challenges from the Data
Point of View in Retail Today?


Multi-Channel.
- Mobile
- Brick and Mortar
- Web

They don't have the recommendation system
in place.

Users and their preferences differ very
much. Users require different recommendations
based on realtime behaviour.

Promotions and Product Recommendations
are as relevant as possible and are
served in real-time.

Collaborative Filtering
- An algorithm that considers users
interactions with products, with the
assumption that other users will behave
in the similar ways.

Content Based
- An algorithm that considers similarities
between products and categories of products


.............................

Silos & Polyglot Persistence

Products
-> Inventory
-> Location
-> Category
-> Price
-> Configurations

Customers / Users
-> Purchase
-> Return
-> Review
-> View
-> In-store Purchases
-> Location
[Recommend to other customers]

.........................


- Purchases (Relational DB)
    - Customers -> Purchase
- Product Catalogue (Product Catalogue)
    - Products -> Category
    - Products -> Price
    - Products -> Configurations
- In-Store Purchase (Relational DB)
    - Customer -> In-store Purchases


Data Lake. Pouring all the data into
this repository. Good for Analytics,
Business Intelligence. Map Reduce.
Non-Operational, Slow Queries

..........................



Purchases (Relational DB)
- Connector

Product Catalogue (Document Store)
- 



Neo4j index free adjacency.
- Realtime Queries.
- Efficiently.


Drivers: Java | JavaScript | Python
.NET | PHP | Go | Ruby


Neo4j.

How can import data from different
data sources, using Cypher -- the
query language for Neo4j -- and
demonstrate both content-based and
collaborative filtering recommendations
using this data.

*/

/*

Neo4j Browser.

*/












