

/*

8 Smart questions to ask hiring managers in a job interview
By Work It Daily
https://www.youtube.com/watch?v=Y95eI-ek_E8&ab_channel=WorkItDaily

....... 4 C's ........

(1) Connect

(2) Culture

(3) Challenges

(4) Close

......................

(1) Connect: Connect with the interviewer.
(2) Connect: How did you come to work here?
(3) Connect: What do you love most about working here?


(1) Culture: See if the culture is right fit for you.
(2) Culture: Who's the most successful recent hire and why?
- Tell me about the most successful hire you've made
recently. Why has that person been very successful in that role?
    - 80 hours a week. No budget. Out of the park. 
(3) Culture: Who didn't succeed as a new hire and why?
- Tell me about a hire recently that didn't work out.
    Why did they fail in their role?
    - Do those traits or characteristics sound like yourself?



(1) Challenges: Find out if you can help to solve the company's challenges
(2) What's the company's biggest challenge this year and how
will this job help overcome it?
- Tell me about the biggest challenge you think the company
will face this year. How will this job help to overcome it?

(3) How will I measure my performance so I know I'm having a
positive impact on this challenge?
- How will I measure my own performance to ensure that
I'm having a positive impact on this challenge?



(1) Close: Figure out what your next steps are.
(2) Close: What additional skills or experience do you wish I had
that would make me a better fit for this job?
- If there were some skills or experience [that] you 
wish I had that would make me a better fit for this
job. What would they be?
(3) Close: What are the next steps in the process?


Phone Call, Follow Up, Make That Decision.

Is there anything else that I need to do to further my
candidacy?

*/
/*

2 Job Interview Mistakes EVERYONE Makes (And How To Avoid Them)
By Work It Daily
https://www.youtube.com/watch?v=6KnJtVnE_FA&ab_channel=WorkItDaily


Tell me about a time...

(1) Not preparing to interview

(2) Not making the interview a 2-way conversation

................................................................

Do you have any questions for us?

*/

/*

How To Answer "Why Do You Want This Job?"
By Work It Daily
https://www.youtube.com/watch?v=-1umUFfIicY&ab_channel=WorkItDaily


(1) They want to know you're not just in it for the money.
(2) They want to know you're not going to leave for a better offer.
(3) They wan to know they can depend on you to do the work.

Connection Story:

What do you respect, appreciate, or admire about this company,
over ANY other company?

The key is to demonstrate an emotional connection with the company.

Tell them why you "get them".

Here's an example.

"I'm really into numbers. I'm really into finance" [no]

"I've been banking at this company for 10 years and every time
I come in here, I'm greeted by friendly people. They 
genuinely care about me. I can tell that they are really 
entrenched in the community. It makes me feel that my money
is safe here and in good hands. And that's the kind of 
organization I want to be a part of. I want to know that
when I'm doing my job, I'm helping people in the community
feel like their money is in a good place. " [yes]

*/
/*
3 Things Hiring Managers Want To Know About You
By Work It Daily
https://www.youtube.com/watch?v=RTvYvZ9VHDc&ab_channel=WorkItDaily


Hiring Managers want (1) Personality (2) Aptitude and (3) Experience.

(1) Share your go-to potential

These are the problems you solve and pains you alleviate.

Expressing this clearly increases your chances of getting hired.

(2) Spell out your value

Explain how you offer value by solving these problems.

(3) Explain your short-term goal

How you're going to take your value and experience
to the next level.

This makes the hirigin manager see your potenetial
as a new hire.




*/







