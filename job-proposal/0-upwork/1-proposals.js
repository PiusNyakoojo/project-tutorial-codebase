
/*
(1) Do the job
(2) Good communicator
(3) Good person to work with
*/
/*
Communication. Responsive. Professional.
*/
/*

[Invited To Repeated Job]

First of all, I am genuinely thankful to you for inviting me for this job. I am truly excited to take on another [Company Name] gig.

I definitely need to know more about the job before I can pass on a detailed solution. May I have a look at the mockup of design? Also, I am interested to know the tools that you are using.

Let's make it a successful project together.

=> My profile

I have been working with [Company Name] since 2017 and

- Done over 50 [Company Name] projects.
- Got 4.5 stars or more stars for all the jobs.
- Working on 4 platforms at the moment can definitely share references and testimonials.
- In these 2 years, I got to work with course entrepreneurs to Fortune 500 companies and celebrities.
- I have 7 years of generic WordPress experience.

Please let me know if you have got any questions.

The following are some of my [Company Name] related projects

website.com

[Website Name] was developed using a ready made theme and design but still ensuring a great user flow was crucial part of the project. We settled for Active campaign for marketing automation and used uncanny tools for the UX and functionality part. WooCommerce has been used as a cart to sell courses.

website.com

We used Paid Memberships Pro as a cart on website.com and for the design part we settled with elementor.

website.com

It's a training system for the Henry ford employees. Although looks quite simple, but has a lot of functionality to offer once a user is logged in it's set to launch in March 2020, but development has been completed. It uses gravity forms + uncanny tools + uncanny codes for the registration process and group management. We have used Gamipress as a gamification system for the users. In order to ensure effective email engagement, infusionsoft has been used on this platform.

No cart has been integrated since codes will be issued individually to each employee by their respective team leaders.
*/
/*

Hello there,

I would like to offer my skills to work on your website.

I have extensive experience and specialization in the setup, configuration and customization of wordpress and Learndash LMS systems. I. Have expertise in handling LMS plugins and have worked with Leaerndash, buddy press, Memberium, Infusionsoft, Uncanny Owl, Divi, Woo-commerce and Membership based websites. I h ave immense experience with LMS compatible themes i.e. Boss theme, Social Learner and eLumine theme. I can help you develop the good website as per your requirements with good standards and practices.

For LMS based websites, I can help you with -

Expertise Skills -
==================
1 ) Integration of Theme and customization with WPLMS and Learndash
2 ) Courses creation
3 ) Quiz creation setup
4 ) Certificate generation, Achievements, badges and reward system.
5 ) Setting up courses for sale/purchase through woo-commerce or Memberships.
6 ) Set up Buddy-drive and BuddyPress
7 ). Integration of membership plugins (Paid Membership Pro, s2 membership)
8 ) Integration of Memberium and Infusionsoft
9 ) Integration of Payment gateway Methods i.e. PayPal Proro, Stripe, Epay, Authorize.net, 1 Shopping cart and much more
10 ) Integration of woo-commerce and customization

Have a look on Learndash Projects -
==========================
1. https://passion.edu
I have used the boss theme with Learndash for this website. I have created the different membership levels I.e. Free, Gold, Platinum. Also integrated the Memberium and Infusionsoft for the payments system. All the contacts / users are synchronized with infusionsoft for webinars and campaigns.

2. https://3dheals.com
I have created this website in elumine theme with Learndash plugin. I have customized the Learndash plugin as per the website requirements.

*/
/*

Who You Are - Introduction
Body - Past Experiences
Conclusion - Explore Further

*/
/*
- Show That You're Friendly and a good person to work with
- Try to include something from the job proposal
- Stress that I'm a good communicator.
*/
/*
Hello, my name is Taylor Wilkinson and I am a front-end focused
freelance web developer with experience using PHP, as well as
building custom WordPress sites and themes.

I am relatively new to Upwork, but I assure you that I take
every job seriously. I would love to build your WordPress
website for your supplement brand. I think you've chosen
a couple of excellent themes. I enjoy working with WordPress
and WooCommerce and this is a project I would be excited
about. I always make sure my clients are satisfied with the
work I do.

Lastly, I take great pride in my prompt and professional
communication. I would love to discuss the job with you
further and hope to hear from you soon.

Thank you,

Taylor Wilkinson
*/
/*

Hello, my name is _______ and I am a web developer with
experience building custom WordPress sites and themes.

I  assure you that I take every job seriously. I would love
the opportunity to build your WordPress website and theme for you.
I do my absolute best to be a good person to work with and to meet
deadlines. I also pay close attention to the details.

I have experience with _____, _____ and ______ (more details about
why you're the right person for the job)

I am committed to my clients and their projects from the moment
the contract begins. I always make sure my clients are satisfied
with the work I do.

Lastly, I take great pride in my prompt and professional
communication. I would love to discuss the job with you further
and hope to hear from you soon. Thank you!

Warm Regards,

________________

*/
