
/*


Elements of a Winning Proposal, in 2021 and Beyond.

Elements of a Winning Proposal

By Danny Margulies.

Being able to write a great proposal is absolutely
key.

(1) Stand out and get clients to notice you
(event if you're new to Upworks)

(2) Show clients you're a good fit for their job

(3) Position yourself as an authority
(even if you don't have a big portfolio)

(4) Expertly answer "Screening Questions"

(5) Plus, next steps for putting this knowledge into
action.

*/

/*
Personally earned hundreds of thousands of dollars
on Upwork (including over $100,000+ in just 12 months)

My work has been featured in many majoy publications,
including Bloomberg Inc., Business Insider, and more.

Created FreelanceToWin.com, where I share Upwork
tips and strategies with hundres of thousands of
freelancers all over the world.
*/

/*

What to expect in 2021 And Beyond

(1) Clients are busier than ever

(2) Clients have more choices than ever

(3) Clients have higher expectations than ever
("paradox of choice")


2021 And Beyond: The Upside

(1) More clients and projects than ever before!

(2) More opportunities to stand out and impress clients

(3) The best way to do that is with a powerful, succinct Proposal


Proposals: My Big Picture Gameplan

(1) I'm trying to start a conversation, not close a deal

(2) My goal is to get a response and then discuss the
project further

(3) I've spent years streamlining my approach
for these purposes


I can't really know if the project is a good fit for me
until I talk to the client. Explore until I get a good
fit. Succinct proposal.

Overview: 3 Basic Elements of Winning Proposals

(1) Brief intro
(2) Proof you can do a great job for the client
(3) A clear call to action

Winning Proposal Element #1: Brief Introduction

Brief Introduction: Goals

(1) Grab attention
(2) Start building rapport
(3) Show I've read the job post

"The Personal Connection Introduction"

(1) Spontaneous statemenet that's unique to the client/situation
(2) Think: "conversation starter"
(3) It can be a genuine compliment, statemenet of empathy,
highlight something you have in common, etc.

Personal Connection Introduction Example

(1) "Hi there - exciting to hear about your upcoming launch!"

(2) Hi, I'm sorry to hear your website was compromised.

(3) "Hi [Client Name], thank you for the invite!"

Another option: The "Affirmation Approach"

(1) Simply repeat back what the client has said in their
job post

(2) Easy to do

(3) Universally applicable


Affirmation Approach: Examples

(1) "Hi -- I see you're looking for a developer proficient
in Ruby On Rails and AngularJS to complete an existing
project!"

(2) "Hi, I see you're looking for an editor to tighten
up your manuscript..."

(3) "Hi, I see you need help setting up Zapier
automations..."

Brief Introduction: Guidelines

(1) Keep it short

(2) Keep it simple

(3) Don't try too hard

Winning Proposal Element #2: Proof you can do a 
great job

Share At Least One Relevant Example of Your
Work

(1) Shows off your skills in a tangible, concrete
way (vs 'This is my specialty!')

(2) Alleviates one of clients' biggest concerns

(3) Helps you stand out

Objection #1: "But I'm new to freelancing/upwork!"

(1) The work you show doesn't have to be "Upwork work"

(2) It can be something you did for a friend, relative
nonprofit, etc.

(3) It can even be something you did for yourself


Never written a landing page for an app before.

The app does not actually exist but the landing page
copy that I wrote is very real and I used it to get
a few jobs.

Objection #2: "But what if I'm not a Writer/Designer?"

(1) Social media manager - screenshots of great posts

(2) Marketer - examples of funnels you've built

(3) Lawyer - examples of contracts
(with personal info removed)

(4) Excel specialist - examples of spreadsheets

(5) Proofreader - before & after examples


Showing Relevant work Examples Guidelines:

(1) 1 - 3 "hand picked" examples works nicely

(2) Attach them to your Cover Letter, or link to them

(3) Explain what makes the work relevant/special/unique
("Tour Guiding")

*/
/*
Cover Letter Example

Hi there - exciting to hear about your upcoming
re-launch! I recently wrote an email sequence
selling an online class - I'm attaching it
here so you can check it out and get a feel
for my work.

A few key points to notice:

- The copy builds excitement about joining the
class. This is key because people don't just make
decisions based on logic - for best results
it's also important to appeal to them
emotionally.

- The copy paints a picture of "transformation."
People join an online class because they want
to transform themselves/their lives in some
important way... They want to know that you
understand where they're at, and that you 
can help them get to where they want to be.

- The copy doesn't just sell, it also teaches.
This is important because when you're selling
an online class, part of what you're selling
is the fact that you are a good teacher.
So proving that in the email copy goes a long
way.

- The copy makes it easy for readers to say
yes. It offers an incentive for joining now,
while also removing their risk - both key
elements of a winning email campaign.

Can you tell me more about the type of class
you're going to be selling?

Cheers,

Danny

*/
/*
Email series. To help sell an online course.

"TOUR GUIDING" Benefits

Increases the perceived value of your work

Positions you as an authority.

Gets you and the client "on the same page" as
far as what they can expect from you.

TOUR GUIDING: IDEAS AND INSPIRATION

What makes it relevant?

What did you learn from working on it?

What makes it good?

What makes it unique / standout?

What result did it achieve? (even an intangible
    one - "the client was happy and rehired me")

What did someone else say about it? ("testimonial")


IF YOU CAN'T SHOW AN EXAMPLE OF YOUR WORK...

But you honestly believe you are qualified to work
on their project anyway . . an alternative
strategy . . not ideal . . but something to do

Talk about a relevant work experience as well.

The client isn't seeing a completed piece of
work that you've done. Somewhat advanced.

ALTERNATIVE: TALK ABOUT A RELEVANT WORK EXPERIENCE

Tell a story

Talk about a result you achieved

Highlight a testimonial

Share your knowledge/expertise

Offer one or more helpful suggestions
for their project

*/
/*

How to Write a Winning Upwork Job Proposal in 2021
By Upwork
https://www.youtube.com/watch?v=bKwbFuqt528&ab_channel=Upwork
@ 19:00 - 20:00


Cover Letter

Hi, I see you're looking for someone to write
conversion-focused chatbot scripts for a 
car dealership. Before I started copywriting
I worked in the Business Development Center
(BDC) of a very successful dealership, and
one thing I learned was that people were
much more likely to give us their information
(particularly their contact info) if we
gave them a solid reason for needing it.

For example, let's say a customer called in
asking for our "best price" on a particular
vehicle... If we asked for their phone
number outright, they often would not give
it. But if we instead said, "Let me check
with our sales manager on that...before
I do that, can I please have a phone number
to reach you at in case we get
disconnected?" -- they gave it to us just
about every time.

Or, let's say someone called in asking how
much we could give them for their trade in...
We would always use that as an opportunity
to try to get them into the store. For
example, we might generate a quote for them,
followed by something like, "But before our
used car manager can agree to give you
top dollar for your vehicle, he really needs
to see it in person. I'd be happy to 
schedule an appointment for you -- are
you available to bring it in this
afternoon?"

I bring this up because I believe it's
very relevant to the task at hand... I
believe the ideal copywriter for this job
is someone who's both good with words,
but also has a deep understanding of
the psychology of someone shopping for
a car online.

I also think there are lots of
opportunities to use that psychology
in order to help the dealership not
only get more leads, but, equally
important (or even more so), to get
more store traffic!

*/
/*

Hi, I see you're looking for someone
to develop Vue.js and Three.js code
for your application. Before I started
developing applications for the web
I worked as an artificial intelligence
researcher and one thing I learned
was that 


*/
/*

Do you have experience writing on HR,
HR tech, or recruitment topics?

I do! I write a bi-weekly blog on HR topics
here:

I also create a newsletter for every new
blog post which contains some more curated
content around a particular HR topic. This
new format has brought a 500% increase
in leads YOY for my client.

Having worked in Global HR at ____, I 
also got some hands-on experience under
my belt.

*/
/*
What challenging part of this job are you 
most experienced in?

I'm 100% on board with your mission of helping
female entrepreneurs to grow their business.
For most of my clients, I'm more than 'just'
the copywriter. I help them grow their
business by offering strategic advice for
their marketing efforts.

For example, I created a complete online
marketing plan for 2019 for one of my long-term
clients, so they can reach their ambitious goal
of generating 3 times the leads than last year.
I identified new avenues that will increase
their presence and establish the company
as a thought leader in the industry.
One of them is establishing a blog on their
website to improve SEO.

*/



/*

Do you have experience with


*/



/*
Hi there - exciting to hear about your
upcoming JavaScript / Node.js development fix!
I receently fixed issues in a web application
using JavaScript - I'm attaching it here so you
can check it out and get a feel for my work:
*/

/*

Hi, I see you're looking for a Vue.js Developer!

Here is the latest project that I created using
Vue.js, Vuetify and Vuex: https://ecoin369.web.app

................................................

Hi there - exciting to hear about your
upcoming Vue.js development project! I recently
created a web application using Vue.js - 
I'm attaching it here so you can check it
out and get a feel for my work:

https://ecoin369.web.app

A few key points to notice:

- The web application uses Vuetify.js as he
UI/UX component library. This is key because
building a UI/UX component library can
delay the development of a project by
many months without a clear outline for
the design criteria of the product.

- The web application is a digital currency
wallet. Digital currencies are engaging
web applications with the potential to 
attract many millions of users. Users
really want user-friendly, interactive
and engaging websites.

- The web application is optimized for
mobile. Mobile web experiences are 
essential for customers today since
mobile phones are so easily accessible
and easy to use. The web application uses
firebase for a realtime database experience.

- The web application is succinct. The
account page showcases many of the features
a user would want to access. The network
status page showcases information about
the statistics related with other accounts.
Accounts, Statistics, Settings and an
About page are very relevant to any 
modern web application.

Can you tell me more about the type of
web application you're going to be
building?

Cheers,

Pius Nyakoojo




*/

/*


- The web application was an experimental
research project to create a digital currency
with a universal basic income. Experimenting
with digital currencies also inspired the
development of a product resource center,
job employment center and decentralized
currency exchange all on the front page
of the web application to showcase to users
how they can quickly and easily access
features relevant to their account.


*/

/*

Hi there - exciting to hear about your
upcoming JavaScript development fix!
I recently fixed a web application using
JavaScript - I'm attaching it here so
you can check it out and get a feel
for my work:

https://ecoin369.web.app

A few key points to notice:

- The web application has a working
header to show the navigation items
such as "Your Account", "Network Status",
"Settings" and "About". This fix
required working on the Vuetify.js
aspect of the codebase.

- The web application has a working
digital currency transaction system.
This fix required working on the Node.js
daemon and transaction processing system.

- The web application is optimized for
mobile. Mobile web experiences are 
essential for customers today since
mobile phones are so easily accessible
and easy to use. The web application uses
firebase for a realtime database experience.
This fix required working on css query
parameters.

Can you tell me more about the type of
project you're going to be
fixing?

Cheers,

Pius Nyakoojo

*/

/*

Hi there - exciting to hear about your
upcoming Firebase development fix!
I recently fixed a web application using
Firebase - I'm attaching it here so
you can check it out and get a feel
for my work:

https://ecoin369.web.app

A few key points to notice:

- The web application has a working
user authentication system using
Firebase Realtime Database security
rules. The security rules use auth.uid
for specific user paths and protect
users from unauthorized writes to
specific locations in the database.

- The web application has a working
digital currency transaction system.
This fix required working on the Node.js
daemon and transaction processing system.
The realtime database rules use auth.uid
and also auth.token.isAdmin to ensure
only the administrator daemon can publish
updates to transactions.

Can you tell me more about the type of
project you're going to be fixing?

Cheers,

Pius Nyakoojo

*/

/*

3d is awesome

Hi there - exciting to hear about your
upcoming three.js development project! I have
experience creating javascript applications
and tutorials using three.js - I'm attaching
it here so you can check it out and get a feel
for my work:

Make a 3D Multiplayer Game with Three.js and Firebase
https://www.youtube.com/watch?v=FdaRPivIq3I&list=PLAKqWAegrR6Nht2iCs4Trpg2GghZogb_f

A few key points to notice:

- The application showcases a multiplayer
game built with Firebase to teach advanced
skills like multiplayer networking.

- The application uses OrbitControls:
a popular javascript library for 3D
rotation in a scene around player characters.
Popular games like World of Warcraft and
Call of Duty have Orbit Controls for rotating
around 3D characters.

- The application is succinct. Multiplayer
networking focuses on having the players
share information about one another's
location and orientation. Topics like
importing textures and animations were
not introduced to keep the tutorial easy
to access without introducing longer videos
or videos that didn't quickly address
the subject matter.

Does my experience with creating three.js
tutorials help in developing the application?

Can you tell me more about the type of
application you're going to be building?

Cheers,

Pius Nyakoojo

*/
/*

Yes. I already have experience with three.js.

I have made
(1) A web application called "Pengpal" which shows a 3D globe for helping people find pen pals to share language knowledge with. https://www.youtube.com/watch?v=45MB_Yb1mnI
(2) A web application called "Magic In the Web" which showcases a demonstration of "Magic the Gathering", a popular card game, working in the web browser using Three.js. https://www.youtube.com/watch?v=9mOgwnfaSws
(3) A web application called "Starflighter" which showcases a test with integrating WebSockets with a touch-screen phone and controlling a three.js spaceship using virtual analogue sticks on the phone. https://www.youtube.com/watch?v=RCNbJab0i7E

I have also created other experiments like trying to recreate Oz from the movie "Summer Wars" as showcased in this video: https://www.youtube.com/watch?v=kPTfdO5hQUw
*/

/*

Single Focus Prove Branding System.

Applied to Proposals on Upwork

(1) Emotional Hook
(2) Personal Introduction
(3) Proof
(4) Qualifications
(5) Disqualification
(6) CTA
*/
/*

*/






