// This is your test secret API key.
const stripe = require('stripe')('sk_test_51KiRu7EsHvDtwnCFCrsZ4LEAvMquhUHUasVXKEJsakMZ7b2FAb0pBUyX5N9JY5ecJ9T5MChwgi5xI1oJAjCyA2sJ001osrNBvS');
const express = require('express');
const app = express();
app.use(express.static('public'));

const YOUR_DOMAIN = 'http://localhost:4242';

app.post('/create-checkout-session', async (req, res) => {
  const session = await stripe.checkout.sessions.create({
    line_items: [
      {
        // Provide the exact Price ID (for example, pr_1234) of the product you want to sell
        price_data: {
          currency: 'USD',
          product_data: {
            name: 'World of Warcraft'
          },
          unit_amount: 10 * 100,
          tax_behavior: 'inclusive'
        },
        quantity: 1
      }
    ],
    mode: 'payment',
    success_url: `${YOUR_DOMAIN}/success.html`,
    cancel_url: `${YOUR_DOMAIN}/cancel.html`,
    automatic_tax: {enabled: true},
  });

  res.redirect(303, session.url);
});

app.listen(4242, () => console.log('Running on port 4242'));