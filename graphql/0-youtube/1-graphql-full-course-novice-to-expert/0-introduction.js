
/*

GraphQL Full Course - Novice to Expert
By freeCodeCamp.org
https://www.youtube.com/watch?v=ed8SzALpx1Q&ab_channel=freeCodeCamp.org

*/
/*

- GraphQL is a powerful query language
- Allows for a more flexible & efficient approach
than REST

.................................................

A RESTful Approach...

- Endpoint for getting a particular book:
domain.com/books/:id
title, genre, reviews, authorId

- Endpoint for getting the author infor of that book:
domain.com/authors/:id
name, age, biography, bookIds

..................................................

*/
/*

A GraphQL Approach...

- Query to get book data and it's author data
(AND the other books):

{
  book(id: 123) {
    title
    genre
    reviews
    author {
      name
      bio
      books {
        name
      }
    }
  }
}

*/
/*

{
  book(id: 123) {
    title
    author {
      name
      books {
        name
      }
    }
  }
}


*/
/*

REST API Endpointts

Get all books:
domain.com/books

Get a single book:
domain.com/books/:id

Get all authors:
domain.com/authors

Get a single author:
domain.com/authors/:id

*/
/*

Walking the Graph...

Author
name
age


Book
title
genre
*/
/*

Project Overview

- Server (Node.js)
  - Express App
  - GraphQL Server

- Database
  - MongoDB (using mLab)

- Client (browser)
  - React App
  - Apollo

- Graphiql
*/
/*

{
  books{
    name
    genre
    id
  }
}

*/
/*

{
  books{
    name
    genre
    author{
      name
      age
    }
  }
}

*/
/*
import {gql} from 'apollo-boost'

const getAuthorsQuery = gql`
{
  auhors{
    name
    id
  }
}
`
*/




