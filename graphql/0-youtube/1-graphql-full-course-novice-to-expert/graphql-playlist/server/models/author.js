
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const authorSchema = new Schema({
  name: String,
  age: Number
})

const model = mongoose.model('Author', authorSchema)

module.exports = model

