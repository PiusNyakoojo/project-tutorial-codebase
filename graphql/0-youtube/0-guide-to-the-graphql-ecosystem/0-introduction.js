

/*

Guide to the GrarphQL Ecosystem
By Ben Awad
https://www.youtube.com/watch?v=VnG7ej56lWw&ab_channel=BenAwad

The GraphQL Landscape

Overview to all the differen tools that are in
the GraphQL Ecosystem.

(1) Programming Languages

- JavaScript / TypeScript
- Go
- Ruby
- Scala
- Elixir
- Python

(2) Schema-first

- One popular way, and one that's less popular.
- Apollo made popular schema first.

*/
/*

type Query {
  posts: [Post]
}

type Post {
  title: String
  text: String
  author: User
}

type User {
  name: String
  followers: [User]
}

*/
/*

const resolvers = {
  Query: {
    posts: () => []
  },
  Post: {
    author: () => { }
  },
  User: {
    followers: () => []
  }
}
*/

/*
Code-first

Query = objecType('Query', t => {
  t.string('hello', {
    args: { name: stringArg() },
    resolve: (_, { name }) => `Hello ${name || `World`}!`,
  })
})
*/
/*
Another Code-first

@Resolver()
class HelloResolver {
  @Query(() => String)
  async hello(@Arg('name') name: string) {
    retturn 'Hello' + name;
  }
}
*/
/*

Recommend starting with the Schema-first approach.

Then later switch to code-first
(GraphQL, Nexus/TypeGraphQL)

*/
/*

Schema-first or Code-first
- Start with schema-first (Apollo/graphql-tools)
- Then later switch to code-first (Graphql
  Nexus/TypeGraphQL)

If you're using TypeORM -> TypeGraphQL
If you're using Prisma -> Graphql Nexus
Neither? Just use the one which you like the syntax

*/
/*

Servers
- Apollo Server
  - Express
  - Fastify
  - Koa
  - Hapi
- GraphQL Yoga -> Yoga2
- Express GraphQL
- NestJS

*/
/*
Middleware that makes GraphQL Stuff easier.
*/
/*

Databases
- Postgres
- MongoDB
- ElasticSearch
- FaunaDB
- Neo4j

You can pick whatever data source you like.
*/
/*
What are your data requirements for your project.
It's a plus if they have a GraphQL integration.
*/
/*
ORM
- Sequelize
- Mongoose
- Objection.js
- Knex.js
- TypeORM
*/
/*

Changing a field in the database. Changes a field
in the graphql schema.

TypeORM has a nice integration with a library.
*/
/*

@ObjectType
@Entity('users')
export class User {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  id: string
  lastName: string

  @Field()
  @Column('text', { unique: true })
  email: string

  @Column('text')
  password: string
}
*/
/*
N + 1 Problem

query {
  allBooks {
    title
    author {
      firstName
      lastName
    }
  }
}

SELECT * FROM Book
SELECT * FROM Author WHERE authorId = 1
SELECT * FROM Author WHERE authorId = 2
SELECT * FROM Author WHERE authorId = 3
...

..........................................

- DataLoader
- Join Monster
*/
/*
GraphQL Services

- AWS AppSync
- PostGraphile
- Hasura
- Prisma ->  Prisma 2
*/
/*
Frontend Tools (React)
- Apollo Client
- Relay
- URQL
- graphql-react
- mst-gql
*/
/*
Codegen
- Apollo CLI
- GraphQL Code Generator
*/
/*
https://github.com/withspectrum/spectrum
*/




