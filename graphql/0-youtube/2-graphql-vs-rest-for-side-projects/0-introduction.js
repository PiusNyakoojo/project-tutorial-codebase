
/*

GraphQL vs REST

Advantages:
- No over/under fetching
- Don't have to version an api
  - api.example.com/v1/posts
  - api.example.com/v2/posts
- It's a type system
  - catches silly errors
  - self documenting
  - tooling
*/
/*

Disadvantages
- More complex to fetch data efficiently
  - might have to use things like dataloader
  - prisma/hasura/postgraphile/appsync/etc
- Harder to cache and rate limit
  - cache with redis
- Request monitoring
- There's no Ruby on Rails or Django yet

*/

