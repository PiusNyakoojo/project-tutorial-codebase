
/*

GraphQL N+1 Problem
By Ben Awad
https://www.youtube.com/watch?v=uCbFMZYQbxE&ab_channel=BenAwad

*/
/*

N + 1 Problem comes up when you are working with
graphql server.

Depending on how you set up your resolver for this,
you may run into the N = + 1 problem.

*/

