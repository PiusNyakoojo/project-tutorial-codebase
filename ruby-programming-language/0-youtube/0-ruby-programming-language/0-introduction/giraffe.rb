
# Printing

# puts "   /|"
# puts "  / |"
# puts " /  |"
# puts "/___|"


# Variables

# character_name = "John"
# character_age = "35"

# puts ("There once was a man named " + character_name)
# puts ("he was " + character_age + " years old.")

# character_name = "Tom"

# puts ("He really liked the name " + character_name)
# puts ("but didn't like being " + character_age + ".")


# Data Types

# name = "Mike" # strings
# occupation = "Programmer"
# age = 75 # integer number
# gpa = 3.258 # floating point number
# isMale = true # boolean
# isTall = false
# flaws = nil # nil

# Working with Strings

# puts "Giraffe\" Academy"
# puts "Giraffe\n Academy"

# phrase = "Giraffe Academy"
# puts phrase

# puts phrase.upcase()
# puts phrase.downcase()

# phrase = "    Giraffe Academy    "
# puts phrase.strip()

# puts phrase.strip().length()

# phrase = "Giraffe Academy"
# puts phrase.include? "Academy"
# puts phrase[0]
# puts phrase[1]
# puts phrase[2]
# puts phrase[3]

# phrase = "Giraffe Academy"
# puts phrase[1, 3]
# puts phrase.index("ffe")
# puts "programming".upcase()

# https://ruby-doc.org/core-3.1.1/String.html
# https://www.rubyguides.com/2018/01/ruby-string-methods/

# Math and Numbers

# num = -20.487

# puts 5
# puts 2**3
# puts 10 % 3

# puts("my fav num " + num.to_s())
# puts num.abs()
# puts num.round()
# puts num.ceil()
# puts num.floor()

# puts Math.sqrt(36)
# puts Math.log(1)
# puts 1 + 7 # integer
# puts 1.0 + 7 # floating point
# puts 10 / 7 # integer
# puts 10 / 7.0 # floating point

# User Input

# puts "Enter Your Name: "
# name = gets.chomp()
# puts ("Hello " + name + ", you are cool!")


# puts "Enter Your Name: "
# name = gets.chomp()
# puts "Enter Your Age: "
# age = gets.chomp()
# puts ("Hello " + name + ", you are " + age)


# Calculator Program

# puts "Enter a number: "
# num1 = gets.chomp()
# puts "Enter another number: "
# num2 = gets.chomp()

# puts (num1.to_i + num2.to_i)
# puts (num1.to_f + num2.to_f)

# Building a Mad Libs Game

# puts "Enter a color: "
# color = gets.chomp()
# puts "Enter a plural noun: "
# plural_noun = gets.chomp()
# puts "Enter a celebrity: "
# celebrity = gets.chomp()

# puts ("Roses are " + color)
# puts (plural_noun + " are blue")
# puts ("I love " + celebrity)


# friends = Array["Kevin", "Karen", "Oscar"]
# puts friends
# puts friends[2] # Oscar
# puts friends[-1] # Oscar

# friends[0] = "Dwight"
# puts friends

# friends = Array.new

# friends[0] = "Michael"
# friends[5] = "Holly"

# puts friends
# puts friends.length()
# puts friends.include? "Karen"

# friends.reverse()
# friends.sort()

# Hashes. Dictionaries.

# states = {
#  :Pennsylvania => "PA",
#  "New York" =>  "NY",
#  "Oregon" => "OR"
# }

# puts states
# puts states["New York"]
# puts states[:Pennsylvania]

# def sayhi
#  puts "Hello User"
# end

# puts "Top"
# sayhi
# puts "Bottom"

# def sayhi(name = "no name", age = -1)
#   puts ("Hello " + name + ", you are " + age.to_s)
# end

# sayhi("Mike")

# Return Statement

# def cube(num)
#   num * num * num
# end

# puts cube(3)

# def cube(num)
#  return num * num * num
#  puts "hello"
# end

# puts cube(3)

# def cube(num)
#   return num * num * num, 70
# end

# puts cube(3)[1]

# If Statements

# ismale = false
# istall = true

# if ismale and istall
#   puts "You are a tall male"
# elsif ismale and !istall
#   puts "You are a short male"
# elsif !ismale and istall
#   puts "You are not male but are tall"
# else
#   puts "You are not male and not tall"
# end

# If Statements Continued

# def max(num1, num2, num3)
#   if num1 >= num2 and num1 >= num3
#     return num1
#   elsif num2 >= num1 and num2 >= num3
#     return num2
#   else
#     return num3
#   end
# end

# puts max(100, 20, 3)

# Building a Better Calculator

# puts "Enter first number: "
# num1 = gets.chomp().to_f

# puts "Enter operator: "
# op = gets.chomp()

# puts "Enter second number: "
# num2 = gets.chomp().to_f


# if op == "+"
#   puts (num1 + num2)
# elsif op == "-"
#   puts (num1 - num2)
# elsif op == "/"
#   puts (num1 / num2)
# elsif op == "*"
#   puts (num1 * num2)
# else
#   puts "Invalid operator"
# end


# Case Expressions

# def get_day_name(day)
#   day_name = ""
# 
#   # if day == "mon"
#   #   day_name = "monday"
#   # elsif day == "tue"
#   #   day_name = "tuesday"
#   # end
#   
#   case day
#   when "mon"
#     day_name = "Monday"
#   when "tue"
#     day_name = "Tuesday"
#   when "wed"
#     day_name = "Wednesday"
#   when "thu"
#     day_name = "Thursday"
#   when "fri"
#     day_name = "Friday"
#   when "sat"
#     day_name = "Saturday"
#   when "sun"
#     day_name = "Sunday"
#   else
#     day_name = "Invalid abbreviation"
#   end
# 
#   return day_name
# end

# puts get_day_name("hello")


# index = 1
# while index <= 5
#   puts index
#   # index = index + 1
#   index += 1
# end

# Building a Guessing Game

# secret_word = "giraffe"
# guess = ""

# while guess != secret_word
#   puts "Enter guess: "
#   guess = gets.chomp()
# end

# puts "You won!"

# Guessing Game with Limitation

# secret_word = "giraffe"
# guess = ""
# guess_count = 0
# guess_limit = 3
# out_of_guesses = false

# while guess != secret_word and !out_of_guesses
#   if guess_count < guess_limit
#     puts "Enter guess: "
#     guess = gets.chomp()
#     guess_count += 1
#   else
#     out_of_guesses = true
#   end
# end

# if out_of_guesses
#   puts "You Lose"
# else
#   puts "You Won!"
# end


# For Loops


# friends = ["Kevin", "Karen", "Oscar", "Angela", "Andy"]
# puts friends[1]

# for friend in friends
#   puts friend
# end

# friends = ["Kevin", "Karen", "Oscar", "Angela", "Andy"]

# friends.each do |friend|
#   puts friend
# end


# for index in 0..5
#   puts index
# end

# 6.times do |index|
#   puts index
# end


# Exponent Method

# def pow(base_num, pow_num)
#   result = 1
#   pow_num.times do |index|
#     result = result * base_num
#   end
#   return result
# end

# puts pow(4, 3)

# Comments

=begin

Comment Block

=end

# Reading Files

=begin

File.open("files/employees.txt", "r") do |file|
  puts file
  # puts file.read()
  # puts file.read().include? "Jim"
  # puts file.readline()
  # puts file.readchar()
  for line in file.readlines()
    puts line
  end
end

=end

# file = File.open("files/employees.txt", "r")
# puts file.read
# file.close()

# Writing Files

# File.open("files/employees.txt", "a") do |file|
#   file.write("\nOscar, Accounting")
# end

# File.open("files/employees.txt", "w") do |file|
#   file.write("Oscar, Accounting")
# end

# File.open("index.html", "w") do |file|
#   file.write("<h1>Hello</h1>")
# end



# Building a Quiz

class Question
  attr_accessor :prompt, :answer
  def initialize(prompt, answer)
    @prompt = prompt
    @answer = answer
  end
end


p1 = "What color are apples?\n(a)red\n(b)purple\n(c)orange"
p2 = "What color are bananas?\n(a)pink\n(b)red\n(c)yellow"
p3 = "What color are pears?\n(a)yellow\n(b)green\n(c)orange"

questions = [
  Question.new(p1, "a"),
  Question.new(p2, "c"),
  Question.new(p3, "b")
]

def run_test(questions)
  answer = ""
  score = 0
  for question in questions
    puts question.prompt
    answer = gets.chomp()
    if answer == question.answer
      score += 1
    end
  end
  puts ("You got " + score.to_s + "/" + questions.length().to_s)
end


run_test(questions)

