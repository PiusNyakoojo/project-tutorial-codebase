

/*

Write a function `canSum(targetSum, numbers)` that
takes in a targetSum and an array of numbers as
arguments.

The function should return a boolean indicating
whether or not it is possible to generate the
targetSum using numbers from the array.

You may use an element of the array as many times
as needed.

You may assume that all input numbers are nonnegative.


Dynamic Programming - Learn to Solve Algorithmic Problems & Coding Challenges
By freeCodeCamp.org
https://www.youtube.com/watch?v=oBt53YbR9Kk&ab_channel=freeCodeCamp.org
*/



function canSum (n, array, memo = {}) {
    if (memo[n] != undefined) return memo[n]
    if (n === 0) return true
    if (array.length === 0) return false

    let isCanSum = false
    for (let i = 0; i < array.length; i++) {
        if (n < array[i]) {
            continue
        }
        isCanSum = isCanSum || canSum(n - array[i], array.filter((a) => a >= array[i]))
    }

    memo[n] = isCanSum

    return memo[n]
}









console.log(canSum(7, [5, 3, 4, 7])) // -> true; 3 + 4, 7

console.log(canSum(7, [2, 4])) // -> false

console.log(canSum(300, [7, 14])) // -> false











