


/*
Dynamic Programming - Learn to Solve Algorithmic Problems & Coding Challenges
By freeCodeCamp.org
https://www.youtube.com/watch?v=oBt53YbR9Kk&ab_channel=freeCodeCamp.org
*/

/*

Write a function `howSum(targetSum, numbers)` that
takes in a targetSum and an array of numbers as
arguments.

The function should return an array containing
any combination of elements that add up to 
exactly the targetSum. If there is no combination
that adds up to the targetSum, then return null.

If there are multiple combinations possible,
you may return any single one.

*/


// function howSum(target, array, memo = {}) {

//     if (memo[target] !== undefined) return memo[target]
//     if (target === 0 ) return []
//     if (array.length === 0) return null

//     let resultList = []
//     for (let i = 0; i < array.length; i++) {
//         let newCurrent = howSum(target - array[i], array.filter((a) => a <= target), memo)
//         if (newCurrent) {
//             // newCurrent.push(array[i])
//         }
//         resultList.push(newCurrent)
//         memo[target - array[i]] = newCurrent
//     }

//     memo[target] = resultList.filter((a) => !!a)[0]

//     // console.log(memo[target])

//     return memo[target]
// }


const howSum = (targetSum, numbers, memo = {}) => {
    if (memo[targetSum]) return memo[targetSum]
    if (targetSum === 0) return []
    if (targetSum < 0) return null

    for (let num of numbers) {
        const remainder = targetSum - num
        const remainderResult = howSum(remainder, numbers, memo)
        memo[remainder] = remainderResult
        if (remainderResult !== null) {
            memo[targetSum] = [...remainderResult, num]
            return memo[targetSum]
        }
    }

    memo[targetSum] = null
    return memo[targetSum]
}




/*

target: 10
array: [1, 2, 3, 4]

10 . . -2 . . 8 
.
. -1
.
9 . . . -2 . . 7
.
. -1
.
8
.
.
.
0 

*/


console.log(howSum(7, [5, 3, 4, 7])) // -> [3, 4], [7]

console.log(howSum(7, [2, 4])) // -> null

console.log(howSum(0, [1, 2, 3])) // -> []

console.log(howSum(8, [2, 3, 5])) // [2, 2, 2, 2]

console.log(howSum(300, [7, 14])) // null






