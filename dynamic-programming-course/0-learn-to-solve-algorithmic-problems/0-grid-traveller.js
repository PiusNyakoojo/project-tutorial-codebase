

/*

Grid Traveller

https://www.youtube.com/watch?v=oBt53YbR9Kk&ab_channel=freeCodeCamp.org

*/


function gridTraversal (m, n, memo = {}) {
    if (memo[`${m},${n}`]) return memo[`${m},${n}`]
    if (m === 0 || n === 0) return 0
    if (m === 1 && n === 1) return 1

    memo[`${m - 1},${n}`] = gridTraversal(m - 1, n, memo)
    memo[`${m},${n - 1}`] = gridTraversal(m, n - 1, memo)

    return memo[`${m - 1},${n}`] + memo[`${m},${n - 1}`]
}

console.log(gridTraversal(18, 18))



