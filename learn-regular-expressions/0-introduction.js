

/*

Regular Expressions (RegEx) -
Define a search pattern that can
be used to search for things in
a string.


Learn Regular Expressions (Regex) - Crash Course for Beginners
By freeCodeCamp.org
https://www.youtube.com/watch?v=ZfQFUJhPqMM&ab_channel=freeCodeCamp.org

freeCodeCamp.org curriculum

- Basic JavaScript
- ES6
- Regular Expressions
- Debugging

*/

/*

Using the Test Method.

Regular expressions are used in programming languages
to match patterns of strings.

let sentence = 'The dog chased the cat.'
let regex = /the/

let myString = 'Hello, World!'
let myRegex = /Hello/
let result = myRegex.test(myString)

*/

// let myString = 'Hello, World!'
// let myRegex = /Hello/
// let result = myRegex.test(myString)

// console.log(result)

/*
Match Literal Strings
*/

// let waldoIsHiding = 'Somewhere Waldo is hiding in this text.'
// let waldoRegex = /Waldo/
// let result = waldoRegex.test(waldoIsHiding) // true

/*
Match a Literal String with
Different Possibilities

"Pipe"
*/

// let petString = 'James has a pet cat.'
// let petRegex = /dog|cat|bird|fish/
// let result = petRegex.test(petString) // true

/*
Ignore Case While Matching

"Flag"
*/

// let myString = 'freeCodeCamp'
// let fccRegex = /freecodecamp/i
// let result = fccRegex.test(myString)

/*
Extract Matches
*/
// let extractStr = 'Extract the word "coding" from this string'
// let codingRegex = /coding/
// let result = extractStr.match(codingRegex)

// console.log(result)

/*
Find more than the first match

"g" flag

*/
// let testStr = 'Repeat, Repeat, Repeat'
// let ourRegex = /Repeat/g
// testStr.match(ourRegex)

// let twinkleStar = 'Twinkle, twinkle, little star'
// let starRegex = /twinkle/ig
// let result = twinkleStar.match(starRegex)

// console.log(result)

/*
Match anything with Wildcard period

. is a wildcard character. It can stand for
anything. 
*/


// let humStr = 'I\'ll hum a song'
// let hugStr = 'Bear hug'
// let huRegex = /hu./
// console.log(humStr.match(huRegex)) // Returns ['hum']
// console.log(hugStr.match(huRegex)) // Returns hug

// let exampleStr = 'Let\'s have fun with regular expressions!'
// let unRegex = /.un/
// let result = unRegex.test(exampleStr)

// console.log(result)

/*
Match Single Character with Multiple Possibilities

square brackets []

bag, big, bug
*/

// let bgRegex = /b[aiu]g/

// let quoteSample = 'Beware of bugs in the above code; I have only proved it correct.'
// let vowelRegex = /[aeiou]/ig
// let result = quoteSample.match(vowelRegex)

// console.log(result)

/*
Match Letters of the Alphabet
*/

// let quoteSample = 'The quick brown fox jumps over the lazy dog'
// let alphabetRegex = /[a-z]/ig
// let result = quoteSample.match(alphabetRegex)

// console.log(result)

/*
Match Numbers and Letters of the Alphabet
*/

// let quoteSample = 'Blueberry 3.141592653s are delicious.'
// let myRegex = /[2-6h-s]/ig
// let result = quoteSample.match(myRegex)

// console.log(result)

/*
Match Single Characters Not Specified

Negated Character Sets.

Match everything except all numbers and all vowels.
*/

// let quoteSample = '3 blind mice'
// let myRegex = /[^0-9aeiou]/ig
// let result = quoteSample.match(myRegex)


// console.log(result)

/*
Match Characters that Occur One or More Times
*/
// let difficultSpelling = 'Mississippi'
// let myRegex = /s+/g
// let result = difficultSpelling.match(myRegex)

// console.log(result)

/*
Match characters that occur zero or more times
*/

// let soccerWord = "gooooooooal!"
// let gPhrase = 'gut feeling'
// let oPhrase = 'over the moon'
// let goRegex = /go*/

// console.log(soccerWord.match(goRegex)) // Returns ['goooooooo']
// console.log(gPhrase.match(goRegex)) // Returns 'g'
// console.log(oPhrase.match(goRegex)) // Returns null

// let chewieQuote = 'Aaaaaaaaaaaarrrgh!'
// let chewieRegex = /Aa*/i
// let result = chewieQuote.match(chewieRegex)

// console.log(result)

/*
Find Characters with Lazy Matching

A greedy match finds the longest possible part
of the string of the regex pattern and returns
it as a match.

A lazy match finds the smallest possible part
of the string and return that.

Regex patterns default to greedy.
*/

// let string = 'titanic'
// let regex = /t[a-z]*i/ // greedy match . . 'titani'
// let regex2 = /t[a-z]*?i/ // lazy match . . 'ti'
// string.match(regex)

// let text = '<h1>Winter is coming</h1>'
// let myRegex = /<.*>/ // <h1>Winter is coming</h1>
// let myRegex2 = /<.*?>/ // <h1>
// let result = text.match(myRegex2)

// console.log(result)

/*
Find one or more criminals in a hunt

This is a basic Regex Challenge.

A group of criminals escape from jail and ran
away but you don't know how many; however,
you do know that they stay close together when
they are around other people. You are responsible
for finding all of the criminals at once.

The criminals are represented by C.
*/

// let crowd = 'P1P2P3P4P5P6CCCP7P8P9'

// let reCriminals = /C+/ig

// let matchedCriminals = crowd.match(reCriminals)
// console.log(matchedCriminals)



/*
Match Beginning String Patterns

You can match patterns that are only at the beginning
of a string.
*/
// let rickyAndCal = 'Cal and Ricky both like racing.'
// let calRegex = /^Cal/
// let result = calRegex.test(rickyAndCal)
// console.log(result)

/*
Match Ending String Patterns
*/

// let caboose = 'The last car on a train is the caboose'
// let lastRegex = /caboose$/
// let result = lastRegex.test(caboose)

// console.log(result)

/*
Match All Letters and Numbers
*/

// let quoteSample = 'The five boxing wizards jump quickly.'
// let alphabetRegexV2 = /\w/g // [a-zA-Z0-9_]
// let result = quoteSample.match(alphabetRegexV2).length

// console.log(result)

/*
Match Everything But Letters and Numbers
*/
// let quoteSample = 'The five boxing wizards jump quickly.'
// let nonAlphabetRegex = /\W/g
// let result = quoteSample.match(nonAlphabetRegex).length

// console.log(result)

/*
Match all numbers
*/

// let numString = 'Your sandwich will be $5.00'
// let numRegex = /\d/g
// let result = numString.match(numRegex).length
// console.log(result)

/*
Match all non-numbers
*/

// let numString = 'Your sandwich will be $5.00'
// let nonNumRegex = /\D/g
// let result = numString.match(nonNumRegex).length
// console.log(result)

/*
Restrict Possible Usernames
*/

/*
1) If there are numbers, they must be at the end.
2) Letters can be lowercase and uppercase.
3) At least two characters long. Two-letter names
    can't have numbers.
*/

// let username = 'JackOfAllTrades'
// let userCheck = /^[A-Za-z]{2,}\d*$/
// // let userCheck = /[a-z]+\d$/i
// let result = userCheck.test(username)

// console.log(result)

/*
Match Whitespace

"\s" is going to match a space, a carriage
return a tab, a form feed and a new line
character

*/
// let sample = 'Whitespace is important in separating words'
// let countWhiteSpace = /\s/g
// let result = sample.match(countWhiteSpace)
// console.log(result)

/*
Match Non-Whitespace characters
*/

// let sample = 'Whitespace is important in separating words'
// let countWhiteSpace = /\S/g
// let result = sample.match(countWhiteSpace)
// console.log(result)

/*
Specify Upper and Lower Number of Matches

Quantity Specifiers.
*/

// let ohStr = 'Ohhh no'
// let ohRegex = /Oh{3,6} no/
// let result = ohRegex.test(ohStr)

// console.log(result)

/*
Specify Only the Lower Number of Matches
*/

// let hasStr = 'Hazzzzah'
// let haRegex = /z{4,}/
// let result = haRegex.test(haStr)

/*
Specify Exact Number of Matches

Quantity Specifier
*/

// let timStr = 'Timmmmber'
// let timRegex = /Tim{4}ber/
// let result = timRegex.test(timStr)

/*
Check for All or None

You can specify the possible existence of an
element with a question mark.

*/

// let favWord = 'favorite'
// let favRegex = /favou?rite/
// let result = favRegex.test(favWord)

// console.log(favRegex)

/*
Positive and Negative Look Ahead

Look Aheads are Patterns that tell
javascript to look ahead in your
string to check for patterns further
along. This can be useful to search
for multiple patterns over the same
string.

Positive look aheads. Negative look
aheads........

A positive look ahead makes sure that
there is a letter after.

A negative look ahead makes sure that
there is not a letter after.

*/

let quit = 'qu'
let noquit = 'qt'
let quRegex = /q(?=u)/ // Positive look ahead
let qRegex = /q(?!u)/ // Negative look ahead
quit.match(quRegex) // Returns "q"
noquit.match(qRegex) // Returns "q"

let sampleWord = 'astronaut'
let pwRegex = /change/
let result = pwRegex.test(sampleWord)










