
/*

- Functional Components
- Props
- Events
    - onClick
    - onBlur
    - onChange
- Hooks
    - useState
    - useEffect
    - useRef

*/

/*
- Functional Components

Your react app is built of various different
JSX components.

import React from 'react'

export const Card = () => {
    return <div>I'm a card</div>
}
*/
/*
- Props

The ability to pass in arguments to a 
react component.

Behind the scenes, this react component is
just a function. Props are arguments to functions
in react.

export const Card = (props) => {
    return <div>I'm a {props.name}</div>
}

function App () {
    return (
        <div className="App">
            <Card name="hello" />
        </div>
    )
}

// OR

function App () {
    return (
        <div className="App">
            <Card name={'hello bob'}
        </div>
    )
}

*/
/*
- Events
    - onClick
    - onBlur
    - onChange

export const Card = (props) => {
    const printHello () => {
        console.log('hello')
    }
    return <div onClick={printHello}>I'm a {props.name}</div>
}

*/
/*
- Hooks
    - useState
    - useEffect
    - useRef

useEffect. When the card component is mounted to the
screen. You could make an http request.

useEffect(() => {
    console.log('we are here')
}, [])

*/




