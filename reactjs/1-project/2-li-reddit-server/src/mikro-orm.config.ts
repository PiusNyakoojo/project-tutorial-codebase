import { __prod__ } from "./constants";
import { Post } from "./entities/Post";


const mikroOrmConfig = {
    entities: [Post],
    dbName: 'lireddit',
    // user: '',
    // password: ''
    type: 'postgresql',
    debug: !__prod__
} as const

export {
    mikroOrmConfig
}



