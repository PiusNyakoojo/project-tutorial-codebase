"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mikroOrmConfig = void 0;
const constants_1 = require("./constants");
const Post_1 = require("./entities/Post");
const mikroOrmConfig = {
    entities: [Post_1.Post],
    dbName: 'lireddit',
    type: 'postgresql',
    debug: !constants_1.__prod__
};
exports.mikroOrmConfig = mikroOrmConfig;
//# sourceMappingURL=mikro-orm.config.js.map