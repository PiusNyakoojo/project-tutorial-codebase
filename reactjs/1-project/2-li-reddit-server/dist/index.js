"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@mikro-orm/core");
const Post_1 = require("./entities/Post");
const mikro_orm_config_1 = require("./mikro-orm.config");
const main = async () => {
    const orm = await core_1.MikroORM.init(mikro_orm_config_1.mikroOrmConfig);
    const post = orm.em.create(Post_1.Post, { title: 'my first post' });
    await orm.em.persistAndFlush(post);
    console.log('-----------sql 2------------');
    await orm.em.nativeInsert(Post_1.Post, { title: 'my first post 2' });
};
main().catch((err) => {
    console.error(err);
});
//# sourceMappingURL=index.js.map