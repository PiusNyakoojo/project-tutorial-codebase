

/*

Cracking the Behavioral Interview for Software Developers
By Keep On Coding
https://www.youtube.com/watch?v=ld0cvWnrVsU&ab_channel=KeepOnCoding

*/
/*

8 to 10 examples from my career that can be used to answer
different styles of questions.

(1) Conflict Management
(2) Good Technical Suggestion

...........................................................

Paint a picture of the solution to the interviewer

...........................................................

(1) Tell me about yourself.

Hey My name is Sam. I'm a software engineer and I enjoy
solving complex problems.

I have experience as a full stack web app developer and 
I enjoy working at all layers of the stack. And I enjoy
building out user friendly efficient websites.

My passion for web app development started when I was
doing my internship at AWS and I was given a solo project
to work on for the entirety of the internship. And this
really gave me full ownership of everything from the
design decisions to the actual coding of it.

And I was also able to gain a lot of experience and
exposure to different of AWS cloud computing tools.

After my internship I decided that this is what I wanted to
do as a career. Since then I've gained about 3 years
of experience building out dozens of different
applications.

At my current company, I'm helping out with the
modernization of our existing infrastructure. This
includes building out new microservices as well as 
updating existing ones.

The technologies I've experienced with include
.NET for building out APIs and our server-side code.
SQL. With MySQL, Oracle and SQL Server. As well as
experience with different various javascript
frameworks including Vue.js and Angular.

So I believe my experience and my passion would make
me a great asset for your company.

*/

/*

Hey My name is Pius Nyakoojo. I'm a software engineer
and I enjoy solving complex problems.

I have experience as a full stack web app developer
and I enjoy working at all layers of the stack. And
I enjoy building out user friendly efficient websites.

My passion for web app development started when I 
built a 3D multiplayer online video game with my
brother over the summer. Working on this project
gave me a lot of ownership of everything from the
design decisions to the actual coding of it.

And I was also able to gain a lot of experience
and exposure to different cloud computing tools
including Firebase and Heroku.

After building out our web game, I decided that
web development was what I wanted to do as a career.
Since then, I've gained about 5 years of experience
building out a handful of different applications

At my current company, I'm helping out with
creating features for an experimental digital
currency. This 


*/

/*

(2) Tell us a time that you had to persuade
someone at work. What did you do?

At my previous company, our team supported 20 to
30 different web applications and there really 
was no uniformity in terms of what front-end
javascript framework we could or should use.

So what ended up happening is we would have
some apps written in angular. Some apps written
in React. Some written in vue.js. It was kind
of all over the place.

And the problem with this. Say you were assigned
to write a feature in a certain app. And you 
didn't really know that framework. You would have
to spend time getting a basic understanding of it
before you could even start to implement that
feature. So this potentially increased the
development time by a lot.

My proposal was to have all of our web applications
be written in Vue.js. Because it was something
that I had experience with. I thought it was
fairly easy to pick up. It's a lightweight app.
It's pretty easy to inject into existing applications.

I created a little mini. Demo. Presentation. And
I set up a meeting with my team and my manager.
And just demonstrated the pros and cons of using
that framework. So the result of that was my manager
was on board and we decided to use Vue.js for
all of our new developments.

*/

/*

At my current company, our team worked on an
experimental digital currency. Digital currencies
have historically not had a web application
javascript front-end where you could manage all
of your money from a single website.

The problem with not having a web application for
managing your money is that users might need
to do additional research into how to acquire
your currency and which wallet they should use
or which extensions they need to download to 
be able to transact.

My proposal was to build a progressive 
web application in Vue.js. Because
Progressive Web Applications and Vue.js
were technologies that I had experience with.
Progressive Web Applications have the advantage
of being like mobile applications since you can
add them to your homescreen and they can work
offline.

My team created a demonstration of a web
application based digital currency and
demonstrated the pros and cons of using
a progressive web application. The result
was that my manager was on board and we
decided to build a progressive web application
for the primary interface of our
new digital currency.

*/

/*

(3) Tell me about a time you failed at work.
And what did you learn from it?


So this one time I was assigned a task of
implementing a new feature into one of our
applications. And we use git as our source
control.

As I was working on this feature, I was
continuously pushing to our master branch.
Which isn't good practice because you
always want to keep your master branch
deployable and in sync with what's in
production. Also if we deployed this app with
this feature partially done, it would break
the application.

But this was an app that barely got upgraded
so I figured we would be alright.

But of course we had an emergency bug fix
that had to be made and I was like "we
actually can't deploy it because the app
is going to break."

Luckily, with source control, you are able
to go back and we were able to add that
change to where I started working on the
feature.

In the future, I learned that in order to..
If I wasn't making any changes, first create
a branch off of master and deploy to that
branch. And then when you're finally done
with the feature, then merge the changes
back into master.

*/
/*




*/








