

/*

Behavioral Interview Questions and Answers

7 BEST Behavioural Interview Questions & Answers!
By CareerVidz
https://www.youtube.com/watch?v=ZLtO_7LjzVg&ab_channel=CareerVidz



Example Questions:

(1) Can you give an example of when you have been flexible in a work related situation?
(2) When you delivered excellent customer service?


Tip: Use the STAR technique.

- Make the task relevant to the job you are applying for.
- Use the following words in your response:

Structure. Time-management. Persistent. Determined.

STAR: Situation. Task. Action. Result.


(1) When have you completed a difficult task or project?

"Yes I can. I recently successfully completed an online customer service training course to further develop my skills in this area and to also fully understand the customers needs. The course took me two months to complete in my own time whilst holding down my current job.

The biggest obstacle I had to overcome was finding the time to complete the work to the high standard that I wanted to achieve. I decided to manage my time effectively and I allocated two hours every evening of the working week in which to complete the studying required. I found the time-management relatively easy to manage as I structured my day accordingly."

"This structured approach enabled me to complete the course quickly and to a high standard."

"I am pleased to say that I passed the course with 100% marks and I now have more experience about the needs of the modern day customer. The things I learned during the course included determining factors that create customer value, identifying the needs of customer and developing strategies to support the modern day customer.

Whenever I approach any type of work-related task I always focus on persistence, being organized and having a determination to complete the job competently and professionally"



(2) Describe a stressful situation at work and how you handled it.

(4) When have you demonstrated flexibility in a work situation?

"As already mentioned, I fully understand how important it is to be
flexible at work and that I might be needed to either cover
someone's illness at short notice or even step in to help
the company as and when required. You can rely on me to be a
flexible and supportive member of your team."


(5) What's been your biggest failure to date and how did you
deal with it?

TIPS...
- Failure is part and parcel of life. It's how you react to the
failure that's important.

- Tell the panel that you view failure as a chance to improve
and develop.


I would say my biggest failure to date is failing an educational
course that I embarked upon approximately two years ago.

The reason why I failed the course was primarily due to a lack
of adequate time set aside for studying. However, I immediately
learnt from my mistakes and shortcomings and I booked a 
re-sit the test immediately.

I worked hard during the build-up to the re-sit and I spent
lots of time studying. I passed the test with flying colours.
I certainly learnt a lot from my initial failure and I always
make sure that I now prepare fully for everything that I do.

It is very important to reflect on your own performance,
especially when things don't go to plan. I now like to view
failure as an opportunity to improve and develop.


(6) Describe a time when you had to deal with somebody in
a work situation who you felt was not pulling their weight
or working as part of the team?
- Be someone who is prepared to tackle people who are not
pulling their weight.
- Never become confrontational, but insteam be calm and
positive in your approach.
- Explain how the member of team is impacting negatively
on the rest of their team -- try to educate them.


"Whilst working in my current role as a waiter for a local
restaurant, I was aware of a colleague who was taking more
breaks than he was entitled to. Whilst he was taking these
additional breaks, the rest of the team would have to cover
for the shortfall. Unfortunately, the customer would then
suffer, as the time it took for them to be served would
increase. I decided to approach the person in order to
resolve the issue.

I walked over to him in a calm manner and asked him in
a friendly manner if he would come and help the rest
of team serve the customers. I told him that we were busy
and that we needed his help.

Fortunately, he responded in a positive manner and realized
that he was taking advantage of his rest periods. Since then,
there has not been an issue. It is important that the 
team gets on and works well together.

We cannot afford to have confrontational situations and
the best way to resolve issues like this is to be honest
and tactful.
"

(7) What would you do if you noticed a work colleague
being harassed or bullied?

TIPS...
- No form of bullying or harassment is acceptable.
- Say you would follow company procedures.
- Intervene in a calm manner and try to educate the
person doing the bullying.

"To begin with, I would certainly take some form of
action. Bullying or harassment should never be
tolerated in the workplace. I would make sure that
I am conversant with the company's policy before taking
action. However, I would do all that I could to stop
the inappropriate behaviour, and that might involve
informing a senior manager.

I would speak to the person who was being bullied or
harassed and do all that I could to support them.
Sometimes those people who are acting as the bully
do not realise what they are doing, and the impact
of their actions. Therefore, it is important to challenge
the person who is carrying out the inappropriate
behaviour but also educate them as what they are doing
wrong and the impact it is having on other people.


"




*/











