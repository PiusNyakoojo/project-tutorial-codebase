

## (1) Can you give an example of when you strengthened a relationship between you and a co-worker? Why was that effective?

### Who?
Jerome

### Context?
Architecture

### What did it effect?
Personal relationship and work relationship.

### Why was it effective?
Easier to work with. Easier to reach out to. Easier to do knowledge shares with.

So I do remember one co-worker who shared an iterest in architecture with me. I'd often visit his desk and he visit mine, umprompted and happy to do so. Once this routine of bouncing off discussion, laughs, and general intrigue had grown and had time to become habit, this made a lot of the work we had to do just as habitual in nature - easy, less stressful, faster, and sometimes second nature - which is what is what most modern software metholodigies will tell you to aim for (i believe it's specifcally called "psychologcial safety" in DevOps)




## (2) Give an example of when you helped a co-worker.


### Situation

Zach was having a problem with updating his css properties to match the results that he was expecting.

### Task

Zach came to me for help. The task was to complete the css challenge which if I remember correctly was centering or aligning some text on the page.

### Action

I updated the source code in Zach's codebase to match something that he was expecting.

### Result

Zach was happy that I helped him with the problem.


### Story

One of my co-workers was having a problem with CSS in regards to meeting the design expectations, and the task was to have a UI element aligned on the center of the page. He had already done his set of troubleshooting and explained to me how he reached this roadblock. With what I was given in regards to what he already tried, I was able to take into account what was already tried, and what I should try next. In this situation, I had the prior knowledge to know what property value he needed to complete his task, but if I hadn't I would've met up with him and discussed troubleshooting I had done, and what solutions we could come up with next. After we had worked through this issue together, we were able to move on in the sprint and get closer to what our client was asking of us at the time.



## (3) When have you completed a difficult task or project?



















(4) Describe a stressful situation at work and how you handled it.
(5) Describe a time when you embraced change within an organization.
(6) When have you demonstrated flexibility in a work situation?
(7) What's been your biggest failure to date and how did you deal with it?
(8) Describe a time when you had to deal with somebody in a work situation who you felt was not pulling their weight or working as part of the team?
(9) What would you do if you noticed a work colleague being harassed or bullied?








