
/*
https://www.youtube.com/watch?v=pmt0aaquh4o&ab_channel=DevOnDuty
*/

class Employee {
    constructor (name) {
        this.name = name
    }

    greet () {
        console.log('Hello, my name is: ', this.name)
    }
}

const getEployeeOption = () => {
    return Math.random() < 0.5 ? new Employee('David') : undefined
}

const getEmployeeResult = () => {
    if (Math.random() < 0.5) throw new Error('Wrong Credentials')
    return new Employee('David')    
}

const employeeOption = getEmployeeOption()
if (employeeOption) employeeOption.greet()

const employeeResult = getEmployeeResult()
employeeResult.greet()

/*

In Rust:

*/

