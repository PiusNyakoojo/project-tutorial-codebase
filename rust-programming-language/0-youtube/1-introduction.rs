
struct Employee {
    name: String,
}

impl Employee {
    fn greet(&self) {
        println!("Hi, my name is {}", self.name);
    }
}

fn get_employee_option() -> Option<Employee> {
    if rand::random::<bool>() {
        Some(Employee {
            name: "David".into()
        })
    } else {
        None
    }
}

fn get_employee_result() -> Result<Employee, EmployeeError> {
    if rand::random::<bool>() {
        Ok(Employee {
            name: "David".into()
        })
    } else {
        Err(EmployeeError::WrongCredentials)
    }
}


fn main () -> Result<(), EmployeeError> {
    let employee_option = get_employee_option();
    if (let Some(employee) = employee_option) {
        employee.greet();
    }
    let employee_result = get_employee_result();
    employee_result.greet();
    Ok(())
}









