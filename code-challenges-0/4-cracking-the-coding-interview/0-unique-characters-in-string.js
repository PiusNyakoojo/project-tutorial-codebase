
/*


*/

function hasUniqueCharacters (string) {
    let frequencyDictionary = {}

    for (let i = 0; i < string.length; i++) {
        let character = string.charAt(i)
        if (!frequencyDictionary[character]) {
            frequencyDictionary[character] = 1
        } else {
            frequencyDictionary[character] += 1
        }
    }

    for (let character in frequencyDictionary) {
        if (frequencyDictionary[character] > 1) {
            return false
        }
    }

    return true
}


// console.log(hasUniqueCharacters('abca'))


/*
Implementation from the book
*/

/*

public boolean isUniqueChars2 (String str) {
    if (str.length() > 256) return false;

    boolean[] char_set = new boolean[256];
    for (int i = 0; i < str.length; i++) {
        int val = str.charAt(i);
        if (char_set[val]) { // ALready found this char in string
            return false
        }
        char_set[val] = true
    }
    return true
}
*/

function hasUniqueCharacters2 (string) {
    for (let i = 0; i < string.length; i++) {
        const character1 = string.charAt(i)
        for (let j = i + 1; j < string.length; j++) {
            const character2 = string.charAt(j)
            if (character1 === character2) {
                return false
            }
        }
    }
    return true
}


console.log(hasUniqueCharacters2('abca'))




