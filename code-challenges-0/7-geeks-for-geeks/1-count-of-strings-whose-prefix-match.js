

/*


Count of strings whose prefix match with the
given string to a given length k

https://www.geeksforgeeks.org/count-of-strings-whose-prefix-match-with-the-given-string-to-a-given-length-k/

Given an array of strings arr[] and given some
queries where each query consists of a string
str and an integer k. The task is to find the
count of strings in arr[] whose prefix of length
k matches with the k length prefix of str.

Input: arr[] = {"abba", "abbb", "abbc", "abbd", "abaa",
"abca"}
str="abbg", k = 3
Output: 4
"abba", "abbb", "abbc", "abbd" are the matching string
s.
Input: arr[] = {"geeks", "geeksforgeeks", "forgeeks"}
str = "geeks"
k = 2
Output: 2

*/












