


/*

Count pairs of indices having equal prefix
and suffix sums

https://www.geeksforgeeks.org/count-pairs-of-indices-having-equal-prefix-and-suffix-sums/


Given an array arr[] of length N, the task is to
find the count of pairs of indices (i, j)
(0-based indexing) such that prefix sum of the
subarray {arr[0],...arr[i]} is equal to the
suffix sum of the subarray
{arr[N - 1],...,arr[j]} (0 <= i, j < N)

Examples

arr[] = {1, 2, 1, 1}
Output: 3

Explanation:
The 3 possible pairs of indices are as follows:
1. Pair {0,3}: prefix sum of {arr[0]} = 1. Suffix
Sum of subarray{arr[3]} = 1.


*/



/*

Time Complexity: O(n^2)
Space Complexity: O(1)

*/


function numberOfPairs (array) {
    let result = 0

    for (let i = 0; i < array.length; i++) {
        let prefixSum = array.slice(0, i + 1).reduce((a, b) => a + b)

        for (let j = 0; j < array.length; j++) {
            let suffixSum = array.slice(j).reduce((a, b) => a + b)

            if (prefixSum === suffixSum) {
                result += 1
            }
        }
    }

    return result
}


/*

Time Complexity: O(n^2)
Space Complexity: O(1)

*/


function numberOfPairs2 (array) {
    let result = 0
    let prefixSumMap = {}

    for (let i = 0; i < array.length; i++) {
        let prefixSum = array.slice(0, i + 1).reduce((a, b) => a + b)
        prefixSumMap[prefixSum] = i
    }

    for (let i = 0; i < array.length; i++) {
        let suffixSum = array.slice(i).reduce((a, b) => a + b)

        if (prefixSumMap[suffixSum] !== undefined) {
            result += 1
        }
    }

    return result
}


// console.log(numberOfPairs([1, 2, 1, 1])) // 3


console.log(numberOfPairs2([1, 2, 1, 1])) // 3




