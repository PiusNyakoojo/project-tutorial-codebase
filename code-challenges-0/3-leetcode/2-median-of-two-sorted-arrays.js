

/*

Median of Two Sorted Arrays

Given two sorted arrays nums1 and nums2 of size m and n
respectively, return the median of the two sorted arrays.

The overall run time complexity should be 

O(log(m + n))

Example 1:

Input: nums1 = [1,3], nums2 = [2]
Output: 2.00000
Explanation: merged array = [1, 2, 3] and median is 2.

Example 2:

Input: nums1 = [1,2], nums2 = [3,4]
Output: 2.50000
Explanation: merged array = [1,2,3,4] and median
is (2 + 3) / 2 = 2.5

*/

/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number}
 */
 var findMedianSortedArrays = function(nums1, nums2) {
    let mergedArray = nums1.concat(nums2).sort((a, b) => a - b)
    let medianValue = 0
    
    console.log(mergedArray)
    
    if (mergedArray.length % 2 === 0) {
        const index1 = Math.floor(mergedArray.length / 2) - 1
        const index2 = index1 + 1
        medianValue = (mergedArray[index1] + mergedArray[index2]) / 2
    } else {
        medianValue = mergedArray[Math.floor(mergedArray.length / 2)]
    }
    return medianValue
};






