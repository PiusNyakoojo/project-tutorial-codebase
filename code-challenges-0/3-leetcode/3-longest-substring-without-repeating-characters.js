

/*
Longest Substring Without Repeating Characters
https://leetcode.com/problems/longest-substring-without-repeating-characters/
*/

/*
Given a string s, find the length of the
longest substring without repeating
characters.

Example 1:
Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc",
with the length of 3.

Example 2:
Input: s = "bbbbb"
Output: 1
Explanation: The answer is "b",
with the length of 1.
*/

var lengthOfLongestSubstring = function(s) {
    
    let substringList = []
    
    for (let i = 0; i < s.length; i++) {
        let character1 = s.charAt(i)
        let characterDictionary = {}
        let lastCharacterIndex = i
        
        characterDictionary[character1] = 1
        
        loop2:
        for (let j = i + 1; j < s.length; j++) {
            let character2 = s.charAt(j)
            if (characterDictionary[character2]) {
                break loop2
            } else {
                characterDictionary[character2] = 1
            }
            lastCharacterIndex = j
        }

        const newSubstring = s.substring(i, lastCharacterIndex + 1)

        substringList.push(newSubstring)
    }
    
    substringList = substringList.sort((a, b) => a.length - b.length)
    
    return (substringList[substringList.length - 1] || s).length
};









