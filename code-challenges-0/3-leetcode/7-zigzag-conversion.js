


/*

Runtime: 92 ms, faster than 90.81% of JavaScript
online submissions for Zigzag Conversion.

Memory Usage: 50.1 MB, less than 15.46% of
JavaScript online submissions for ZigZag
Conversion.

var convert = function(s, numRows) {
    if (numRows === 1 || s.length === 1) {
        return s
    }

    const rowList = [...new Array(numRows)].map(() => [])
    const numberUntilTop = (numRows + numRows - 2)
    let resultString = ''
    
    for (let i = 0; i < s.length; i++) {
        const minimumNum = Math.floor(i / numberUntilTop) * numberUntilTop
        const maximumNum = minimumNum + numRows
        const isNotDiagonal = (i >= minimumNum) && (i < maximumNum)

        let rowIndex
        if (isNotDiagonal) {
            rowIndex = i % numberUntilTop
        } else {
            rowIndex = (numberUntilTop) - (i % numberUntilTop)
            
        }
        
        rowList[rowIndex].push(s.charAt(i))
    }
    
    for (let i = 0; i < rowList.length; i++) {
        if (rowList[i].length > 0) {
            resultString += rowList[i].reduce((a, b) => a + b)
        }
    }
    
    return resultString
};


*/



