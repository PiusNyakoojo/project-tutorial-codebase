



/*

Runtime: 97ms, faster than 71.35% of JavaScript
online submissions for Valid Sudoku.

Memory Usage: 46mb, less than 19.43% of JavaScript
online submissions for Valid Sudoku.

Read the comments in the source code below:

var isValidSudoku = function(board) {
    
    for (let i = 0; i < board.length; i++) {
        
        // Track the already used numbers for
        // each row, column and square.
        let rowCountTable = {}
        let columnCountTable = {}
        let squareCountTable = {}

        for (let j = 0; j < board[i].length; j++) {
            const rowNumber = board[i][j]
            const columnNumber = board[j][i]
            const squareNumber = board[Math.floor(i / 3) * 3 + Math.floor(j / 3)][(i % 3) * 3 + j % 3]
            
            // If the row number is already used
            // return false
            if (rowNumber !== '.' && !rowCountTable[rowNumber]) {
                rowCountTable[rowNumber] = 1
            } else if (rowCountTable[rowNumber]) {
                return false
            }

            // If the column number is already used
            // return false
            if (columnNumber !== '.' && !columnCountTable[columnNumber]) {
                columnCountTable[columnNumber] = 1
            } else if (columnCountTable[columnNumber]) {
                return false
            }

            // If the square number is already used
            // return false
            if (squareNumber !== '.' && !squareCountTable[squareNumber]) {
                squareCountTable[squareNumber] = 1
            } else if (squareCountTable[squareNumber]) {
                return false
            }
        }
    }
    
    // The numbers weren't used more than once
    // per row, per column and per square
    return true
};



*/



