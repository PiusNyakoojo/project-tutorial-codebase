

/*

https://leetcode.com/problems/add-two-numbers/submissions/

*/


var addTwoNumbers = function(l1, l2) {
    let number1 = getNumber(l1)
    let number2 = getNumber(l2)
    let sumNumberList = []
    let sumNumberListLength = Math.max(number1.length, number2.length)
    
    if (number1.length > number2.length) {
        const arrayOfZeros = [...new Array(sumNumberListLength - number2.length)].map(() => 0)
        number2 = arrayOfZeros.concat(number2)
    } else if (number2.length > number1.length) {
        const arrayOfZeros = [...new Array(sumNumberListLength - number1.length)].map(() => 0)
        number1 = arrayOfZeros.concat(number1)
    }
    
    
    for (let i = 0; i < sumNumberListLength; i++) {
        sumNumberList.push((number1[i] || 0) + (number2[i] || 0))
    }
    
    for (let i = sumNumberListLength - 1; i >= 0; i--) {
        let number = sumNumberList[i]
        let selfNumber = number % 10
        let addToNextNumber = Math.floor(number / 10)
        
        sumNumberList[i] = selfNumber
        
        if (i - 1 >= 0) {
            sumNumberList[i - 1] += addToNextNumber   
        } else if (addToNextNumber !== 0) {
            sumNumberList.unshift(addToNextNumber)
        }
    }
    
    return getLinkedList(sumNumberList)
};
    
function getNumber(list) {
    let numberList = []
    let list1 = list
    let lengthOfList = getLengthOfList(list1)

    for (let i = 0; i < lengthOfList; i++) {
        numberList.unshift(list1.val)
        list1 = list1.next
    }
    return numberList
}
    
function getLinkedList (numberList) {
    let linkedList = new ListNode(parseInt(numberList[numberList.length - 1], 10))
    let linkedListTemp = linkedList
    
    for (let i = numberList.length - 2; i >= 0; i--) {
        linkedList.next = new ListNode(parseInt(numberList[i], 10))
        linkedList = linkedList.next
    }
    return linkedListTemp
}

function getLengthOfList (list) {
    let list1 = list
    let i = list1.val !== null ? 1 : 0
    while (list1.next !== null) {
        list1 = list1.next
        i++
    }
    return i
}
