

/**
 * @param {string[]} strs
 * @return {string}
 */
 
var longestCommonPrefix = function(strs) {
    let word1 = strs[0]
    let longestPrefixLengthList = []
    
    for (let i = 1; i < strs.length; i++) {
        let word2 = strs[i]
        let longestPrefixLength = 0

        loop2:
        for (let j = 0; j < word1.length; j++) {
            if (word1.charAt(j) !== word2.charAt(j)) {
                break loop2
            }
            longestPrefixLength += 1
        }
        
        longestPrefixLengthList.push(longestPrefixLength)
    }
    
    const minimumPrefix = Math.min(...longestPrefixLengthList)
    
    return word1.slice(0, minimumPrefix)
}



