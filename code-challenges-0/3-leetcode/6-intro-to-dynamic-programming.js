

/*

https://leetcode.com/explore/featured/card/dynamic-programming/630/an-introduction-to-dynamic-programming/4094/

To summarize: If a problem is asking for
the maximum/minimum/longest/shortest of something,
the number of ways to do something, or if it
is possible to reach a certain point, it is
probably greedy or DP. With time and practice,
it will become easier to identify which is the
better approach for a given problem.

Although, in general, if the problem has
constraints that cause decisions to affect
other decisions, such as using one element
prevents the usage of other elements, then
we should consider using dynamic programming
to solve the problem. These two characteristics
can be used to identify if a problem should be
solved with DP.

Note: these characteristics should only be used
as guidelines - while they are extremely
common in DP problems, at the end of the day DP
is a very broad topic.


*/




