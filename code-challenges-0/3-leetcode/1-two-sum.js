
/**
 * @param {number} x
 * @return {boolean}
 */

var isPalindrome = function(x) {
    let string = `${x}`
    
    // track the start of the number
    // and the end of the number
    // loop through the string so long
    // as the characters are equal
    // otherwise, return
    let start = 0
    let end = string.length - 1

    while (start < end && string.charAt(start) === string.charAt(end)) {
        start++
        end--
    }
    
    return start >= end
};



