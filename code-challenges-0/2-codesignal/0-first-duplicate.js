
/*

CodeSignal Interview Prep: Arrays firstDuplicate
By Cupcake Coder
https://www.youtube.com/watch?v=ftom3av5g_8&ab_channel=CupcakeCoder

*/



function firstDuplicate (arr) {
    let frequencyDictionary = {}

    for (let number of arr) {
        if (!frequencyDictionary[number]) {
            frequencyDictionary[number] = 1
        } else {
            return number
        }
    }

    return -1
}

console.log(firstDuplicate([1, 1, 2, 3, 4, 2]))
console.log(firstDuplicate([1, 2, 3, 4, 2, 1]))
console.log(firstDuplicate([1, 2, 3, 4]))



