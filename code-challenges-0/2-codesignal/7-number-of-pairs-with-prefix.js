

/*

Time Complexity: O(n^2)
Space Complexity: O(1)

*/


function countNumberOfPairs (array) {
    let numberOfPairs = 0

    for (let i = 0; i < array.length; i++) {
        for (let j = i + 1; j < array.length; j++) {
            if (array[j].indexOf(array[i]) === 0) {
                numberOfPairs += 1
            }
        }
    }
    return numberOfPairs
}

/*



*/

function countNumberOfPairs2 (array) {
    let numberOfPairs = 0
    let prefixMap = {}

    // Time Complexity: O(n)
    // Space Complexity: O(n)
    const updateMap = (string, map = {}) => {
        if (!string || string.length === 0) {
            return map
        }
        let root = string.charAt(0)
        map[root] = { ...map[root] }
        if (string.length === 1) {
            map[root].isWord = true
        }
        updateMap(string.substring(1), map[root])
        return map
    }

    const numberOfSuffixes = (map = {}) => {

        if (typeof map !== 'object') return 0
        if (Object.keys(map).length === 1 && map.isWord) return 1

        let sum = 0

        for (let prefix in map) {

            let number = numberOfSuffixes(map[prefix])

            sum += number
        }

        return sum
    }

    // Time Complexity: O(N * M)
    for (let i = 0; i < array.length; i++) {
        prefixMap = updateMap(array[i], prefixMap)
    }

    for (let i = 0; i < array.length; i++) {
        // if (array[j].indexOf(prefix) === 0) {
        //     numberOfPairs += 1
        // }
        let map = prefixMap
        // const lastCharacter = array[i].charAt(array[i].length - 1)
        for (let j = 0; j < array[i].length; j++) {
            map = map[array[i].charAt(j)]
        }
        let numberOfWords = (Object.keys(map).length > 1) ? numberOfSuffixes(map) : 0
        // console.log('map: ', numberOfWords, '; ', map)
        numberOfPairs += numberOfWords
    }

    return numberOfPairs
}



console.log(countNumberOfPairs2(['back', 'door', 'backdoor', 'gamon', 'backgamon'])) // 2 : ['back', 'backdoor'], ['back', 'backgamon']


console.log(countNumberOfPairs2([
    'abc', // [abc, abc123] // 1
    'abc123',
    '123', // [123, 123456], [123, 123555] // 2
    '123456',
    'hello-world',
    'hello', // [hello, hello-world] // 1
    'world',
    '123555'
]))



