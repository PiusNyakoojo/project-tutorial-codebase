

/*

Naive approach:

*/

function solution(n) {
    
    let indexQueue = [[0, 0]]
    let indexMap = {}
    let numberOfIndices = 0
    
    while (indexQueue.length > 0) {
        const point = indexQueue.pop()
        const pointKey = point.join(',')
        
        if (point[0] === n) {
            break
        }

        if (indexMap[pointKey]) {
            continue
        }
        indexMap[pointKey] = true

        numberOfIndices += 1        
        
        indexQueue.unshift([point[0] + 1, point[1]])
        indexQueue.unshift([point[0] - 1, point[1]])
        indexQueue.unshift([point[0], point[1] + 1])
        indexQueue.unshift([point[0], point[1] - 1])
    }
    
    return numberOfIndices
}

/*

Count The Size of the corners

*/

function solution(n) {
    let cornerSum = 0

    for (let i = 0; i < n; i++) {
        cornerSum += i
    }
    
    return Math.pow((2 * (n - 1) + 1), 2) - 4 * cornerSum
}





