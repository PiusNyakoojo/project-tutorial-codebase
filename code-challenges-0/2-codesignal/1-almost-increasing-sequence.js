

/*

Given a sequence of integers as an array,
determine whether it is possible to obtain
a strictly increasing sequence by removing
no more than one element from the array.

Note: sequence a0, a1, ..., an is 
considered to be strictly increasing if
a0 < a1 < ... < an. Sequence containing
only one element is also considered to be
strictly increasing.

Example

* For sequence = [1, 3, 2, 1] toe output
should be almostIncreasingSequence(sequence)
= false

There is no one element in this array that
can be removed in order to get a strictly
increasing sequence.

For sequence sequence = [1, 3, 2], the
output should be
almostIncreasingSequence(sequence)
= true

*/



function almostIncreasingSequence (sequence) {
    const sortedArray = [...sequence].sort((a, b) => a - b)

    loop1:
    for (let i = 0; i < sequence.length; i++) {
        const sequence2 = [...sequence.slice(0, i), ...sequence.slice(i + 1)]
        const sortedSequence2 = [...sortedArray.slice(0, i), ...sortedArray.slice(i + 1)]

        console.log(sequence2)

        let isEqual = false
        loop2:
        for (let j = 0; j < sequence2.length; j++) {
            if (sequence2[j] === sortedSequence2[j]) {
                isEqual = true
            } else {
                isEqual = false
                break loop2
            }
        }

        return isEqual
    }

    return false
}



console.log(almostIncreasingSequence([1, 3, 2, 1]))

console.log(almostIncreasingSequence([1, 3, 2]))


/*

Things Learned

(1) Labels on loops
(2) In-place sorting of javascript array can be replaced with
[...array]


*/



