

/*

CERTIFY User Experience
By CodeSignal
https://www.youtube.com/watch?v=cVaqCyYK4zM&ab_channel=CodeSignal

*/

/*

Codewriting

Given a positive integer number n, your task is
to calculate the difference between the product
of its digits and the sum of its digits.

Note: The order matters; the answer should be
of the form product - sum ( and not sum - product)

*/

function digitsManipulations(n) {
    let product = `${n}`.split('').reduce(function (a, b) {
        return parseInt(a, 10) * parseInt(b, 10)
    })
    let sum = `${n}`.split('').reduce(function (a, b) {
        return parseInt(a, 10) + parseInt(b, 10)
    })
    return product - sum
}


console.log(digitsManipulations(123456))



