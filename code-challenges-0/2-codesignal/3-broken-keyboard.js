

/*
Broken Keyboard
*/
/*
https://www.hackerrank.com/contests/dcl2k15/challenges/broken-keyboard/problem
*/

function processData(input) {
    //Enter your code here
    let inputList = input.split('\n')
    let numberOfTestCases = parseInt(inputList.shift(), 10)
    // let resultList = []

    for (let i = 0; i < numberOfTestCases; i += 1) {
        let minimumNumber = 0
        let index1 = 2 * i
        let index2 = index1 + 1
        for (let j = 0; j < inputList[index1].length; j++) {
            const brokenCharacter = inputList[index1].charAt(j)
            if (inputList[index2].indexOf(brokenCharacter) >= 0) {
                minimumNumber += 1
            }
        }
        console.log(minimumNumber)
    }
} 


