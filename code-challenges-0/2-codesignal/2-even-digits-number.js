

/*

Even Digits Number

https://leetcode.com/problems/find-numbers-with-even-number-of-digits/submissions/

*/


/**
 * @param {number[]} nums
 * @return {number}
 */
 var findNumbers = function(nums) {
    let result = 0

    for (let number of nums) {
        result += `${number}`.length % 2 === 0 ? 1 : 0
    }
    return result
};







