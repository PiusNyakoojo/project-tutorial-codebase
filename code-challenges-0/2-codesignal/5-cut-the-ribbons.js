

/*

Cut the ribbons

https://leetcode.com/discuss/interview-question/1112081/codesignal-help

*/

/*

You are given an array of integers a, where each
element a[i] represents the length of the ribbon

Your goal is to obtain k ribbons of the same
length, by cutting the ribbons into as many 
pieces as you want.

Your task is to calculate the maximum integer
length L for which it is possible to obtain at
least k ribbons of length L by cutting the
given ones.

For a = [5, 2, 7, 4, 9] and k = 5, the output
should be cutTheRibbons(a, k) = 4

For a = [1, 2, 3, 4, 9] and k = 6, the output
should be cutTheRibbons(a, k) = 2

*/




