

/*

Evaluator


Question 1 of 20

What is the worst case running
time for finding an element
in a binary tree
(not necessarily binary search
tree) of size n?

- O(nlogn)
- O(1)
- O(logn)
- O(n)
- I don't know [x]

Question 2 out of 20

What is the worst case running
time for finding an element
in a binary search tree
(not necessarily balanced)
of size n?

- O(nlogn)
- O(n)
- O(1)
- O(logn)
- I don't know [x]


Question 3 out of 20

Which algorithm should you use
to find a node that is close
to the root of the tree?

- Breadth first search [x]
- Depth first search
- I don't know

Question 4 out of 20

Which type of traversal
does breadth first search
do?

- Pre-order traversal
- Post-order traversal
- Level-order traversal
- In-order traversal
- I don't know [x]

Question 5 out of 20

What is the best way of
checking if an element exists
in a sorted array once in
terms of time complexity?
Select the ebst that applies.


Question 5 of 20

What is the best way of
checking if an element
exists in a sorted array
once in terms of time
complexity? Select
the best that applies.

- Linear Search
- Quick Select
- Hash Set
- Binary Search [x]
- I don't know

Question 6 out of 20

A person thinks of a number
between 1 and 1000.
You may ask any number
questions to them, provided
that the question can be
answered with either
"yes" or "no".

What is the minimum number
of questions you needed
to ask so that you are
guaranteed to know
the number that the person
is thinking?

- 4
- 8
- 1000
- 9
- 10 [x]
- 11
- 20
- 31
- 32
- I don't know

Question 7 out of 20

Which of the following
problems can be solved with
backtracking (select multiple)

- Generating subsets
- Sorting integers
- Generating random numbers
- Generating permutations

[none selected]


Question 8 out of 20

Which of the following is a
good use of backtracking?

- Topological sorting
- Random number generation
- Matrix multiplication
- Combinatorial problems

- I don't know [x]

Question 9 out of 20

Which of the following uses
divide and conquer strategy?

- Heap sort
- Merge Sort [x]
- Bubble sort
- Insertion sort

- I don't know


Question 10 out of 20

How does quick sort divide
the problem into subproblems?

- Divide the array into two
equal halves by index
- Divide the array into two
based on whether an element
is smaller than an arbitrary
value
- Divide the array into a stray
element and the rest of the
array
- Quick sort does not use
divide and conquer
- I don't know [x]

Question 11 out of 20

What are the two properties
the problem needs to have for
dynamic programming to be
applicable? (Select 2)

- Optimal substructure
- Overlapping subproblems [x]
- Constant time subproblems [x]
- Non-overlapping subproblems




Question 12 out of 20

What is an advantage of top-down
dynamic programming vs bottom-up
dynamic programming?

- It's faster
- It has better space complexity
- It has better time complexity
- Order of computer subproblems
does not matter
- I don't know [x]



Question 13 out of 20

Which of the tree traversal
order can be used to obtain
elements in a binary search
tree in sorted order?

- Level-order traversal
- In-order traversal
- Post-order traversal
- Pre-order traversal
- I don't know [x]

Question 14 out of 20

What's the relationship
between a tree and a graph

- No relationship
- A tree is a special graph
- They are the same thing
- A graph is a special tree [x]
- I don't know


Question 15 out of 20

How would you design a stack which
has a function min that returns the
minimum element in the stack, in
addition to push and pop? All push,
pop, min should have running time
O(1).

- Add a queue to store the minimums
- Add a heap to store the minimums
- Add a variable to track the minimum
- Add another stack to store the minimums
- I don't know [x]


Question 16 out of 20

Given an array of 1,000,000 integers
that is almost sorted, except for 2
pairs of integers. Which algorithm
is fastest for sorting the array?

- Quick sort
- Insertion sort
- Merge sort
- Heap sort
- I don't know [x]

Question 17 out of 20

A heap is a ...?

- Array
- Stack
- Hash Table
- Queue
- Tree
- Linked List
- I don't know [x]

Question 18 out of 20

What's the output of running the
following function using input
[30, 20, 10, 100, 33, 12]?

*/

class HeapItem {
    constructor(item, priority = item) {
        this.item = item;
        this.priority = priority;
    }
}

class MinHeap {
    constructor () {
        this.heap = []
    }

    push (node) {
        // insert the new node at
        // the end of the heap array
        this.heap.push(node);
        // find the correct position
        // for the new node 
        this.bubble_up();
    }
    bubble_up() {
        let index = this.heap.length - 1;

        while (index > 0) {
            const element = this.heap[index];
            const parentIndex = Math.floor((index - 1) / 2);
            const parent = this.heap[parentIndex];

            if (parent.priority <= element.priority) break;
            // if the parent is bigger than the child
            // then swap the parent and child
            this.heap[index] = parent;
            this.heap[parentIndex] = element;
            index = parentIndex;
        }
    }
    pop() {
        const min = this.heap[0];
        this.heap[0] = this.heap[this.size() - 1];
        this.heap.pop();
        this.bubble_down();
        return min;
    }

    bubble_down() {
        let index = 0;
        let min = index;
        const n = this.heap.length;

        while (index < n) {
            const left = 2 * index + 1;
            const right = left + 1;

            if (left < n && this.heap[left].priority < this.heap[min].priority) {
                min = left;
            }
            if (right < n && this.heap[right].priority < this.heap[min].priority) {
                min = right;
            }
            if (min === index) break;
            [this.heap[min], this.heap[index]] = [this.heap[index], this.heap[min]];
            index = min;
        }
    }

    peek () {
        return this.heap[0];
    }
    size () {
        return this.heap.length;
    }
}


function fun(arr) {
    const heap = new MinHeap();
    for (const x of arr) {
        heap.push(new HeapItem(x));
    }
    const res = [];
    for (let i = 0; i < 3; i++) {
        res.push(heap.pop().item);
    }
    return res;
}

/*

fun([30, 20, 10, 100, 33, 12])

// for each element add a heap
item to the min heap.

// Pop the first 3 elements off
of the min heap.

// return an array of the
popped elements.

...............................

How does a minheap work?

...............................

Try inputs [1, 2, 3, 4]

- push 1, 2, 3
- pop 3 times.
- bubble_up
    - heap-length = 1
        - index = 1
    - heap-length = 2
        - index = 1
            - parentIndex = Math.floor((index - 1) / 2)
                = Math.floor((1 - 1) / 2) = 0
        - index = 0
    - heap-length = 3
        - index = 2
            - parentIndex = Math.floor((index - 1) / 2)
                 = Math.floor((2 - 1) / 2) = 0
        - index = 0
    - heap-length = 4
        - index = 3
            - parentIndex = Math.floor((3 - 1) / 2)
                = 1
        - index = 1
        - index = 0

- bubble_down
    - 

*/

/*

- [100, 33, 30]
- [10, 12, 20] [x]
- [30, 20, 10]
- I don't know

*/

/*

Question 19 out of 20

Which two pointer techniques
do you use to check if a string
is a palindrome?

- Fast-slow pointers
- Prefix sum
- Two pointers moving in the
opposite direction [x]
- Sliding window
- I don't know

Question 20 out of 20

Which two pointer technique
does Quick Sort use?

- Fast-slow pointers
- Two pointers moving in the
opposite direction
- Prefix sum
- Sliding window
- I don't know [x]



*/









