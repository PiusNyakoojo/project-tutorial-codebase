
/*

https://medium.com/@eric_lum/the-balanced-brackets-problem-ffc01acee8ee

Eric Lum
May 13, 2018·2 min read


Balanced Brackets Algorithm


I recently got a code challenge to implement
a balanced brackets algorithm.

After looking at how it works for a bit, I
decided to write a guide to understanding and
solving the problem. This is a common problem
that you will see associated with the stack
abstract data type.

Defining the problem
Given a string, determine if it has balanced
brackets. A couple of string examples might be
"This is a [balanced bracket]" or "This [[is not]
a balanced bracket". More interesting examples
might involve multiple types of bracket-like openers
including parentheses and curly brackets such as,
"This ( is a really {interesting}) examples".

Use cases
As you already probably know, some programming
languages use a lot of parentheses, such as
JavaScript. When creating a programming
language, a programmer has to implement a
balanced brackets algorithm into the language
in order to determine if a statement is valid
code according to the syntax of the language.

General approach and solution
One of the easiest ways to solve this problem
is to use the Stack abstract data type. While
I don't think most interviewers will ask about
implementing a stack, it will be good practice
in your chosen language.

The basic approach is as follows: iterate through
the sample string, when the current character
is an opening bracket, push it onto a stack
(or array). Similarly, when there's a matching
closing bracket, pop the corresponding opening
bracket off of the stack. After iterating through
the string, the length of the stack will
determine whether the string contains balanced
brackets.

Below, is an example of a bracket matching
solution I wrote in javascript.

*/

let sample = 'T{his [ i]s( a test) string}'

function balancedB(s) {
    let stack = []

    for (var i = 0; i < s.length; i++) {
        let current = s.charAt(i)

        if (current == '(' || current == '{' || current == '[') {
            stack.push(current)
        } else if (current == '}' || current == ']' || current == ')') {
            switch(stack[stack.length - 1]) {
                // basically just have to pop off when it matches
                case '(':
                    if (current == ')') {
                        stack.pop()
                    }
                    break
                case '{':
                    if (current == '}') {
                        stack.pop()
                    }
                    break
                case '[':
                    if (current == ']') {
                        stack.pop()
                    }
                    break
                default:
                    // to account for when you've got
                    // a lot of starting end brackets
                    if (stack.length == 0) {
                        return false
                    }
                    break
            }
        }
    }

    if (stack.length == 0) {
        // you've dot it -- you've popped off all of
        // the remaining bracket ends
        return true
    } else {
        // you've got some remaining brackets so
        // they're unbalanced.
        return false
    }
}




console.log(balancedB(sample))



