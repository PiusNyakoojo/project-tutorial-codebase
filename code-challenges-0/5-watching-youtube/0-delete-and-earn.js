
/*
740. Delete and Earn

https://leetcode.com/problems/delete-and-earn/submissions/

Delete and Earn - Dynamic Programming - Leetcode 740 - Python
By NeetCode
https://www.youtube.com/watch?v=7FCemBxvGw0&ab_channel=NeetCode

*/


/*
* @param {number[]} nums
* @return {number}

// My Original Approach
// ~ 21 minutes

var deleteAndEarn = function(nums) {
let dictionary = {}
let numberOfPoints = 0

for (let i = 0; i < nums.length; i++) {
    let number = nums[i]
    if (!dictionary[number]) {
        dictionary[number] = {
            frequency: 1
        }
    } else {
        dictionary[number].frequency += 1
    }
}
for (let number in dictionary) {
    dictionary[number].frequencySum = dictionary[number].frequency * parseInt(number, 10)
}

nums = nums.sort((a, b) => {
    return dictionary[a].frequencySum - dictionary[b].frequencySum
})

for (let i = nums.length - 1; i >= 0; i--) {
    let number = nums[i]
    if (!dictionary[number]) {
        continue
    }
    
    console.log(number)
    
    numberOfPoints += number
    
    delete dictionary[number - 1]
    delete dictionary[number + 1]
}

return numberOfPoints
};
*/


/*



*/




