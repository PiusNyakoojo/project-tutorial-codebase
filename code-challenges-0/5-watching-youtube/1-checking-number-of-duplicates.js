


/*

Time Complexity: O(n^2)
Space Complexity: O(1)

*/


function hasDuplicates (array) {
    for (let i = 0; i < array.length; i++) {
        for (let j = i + 1; j < array.length; j++) {
            if (array[i] == array[j]) {
                return true
            }
        }
    }
    return false
}

/*

Time Complexity: O(n)
Space Complexity: O(n)

*/

function hasDuplicates2 (array) {
    let frequencyCount = {}

    for (let i = 0; i < array.length; i++) {
        if (!frequencyCount[array[i]]) {
            frequencyCount[array[i]] = 1
        } else {
            frequencyCount[array[i]] += 1
        }
    }

    for (let number in frequencyCount) {
        if (frequencyCount[number] > 1) {
            return true
        }
    }

    return false
}


console.log(hasDuplicates([1, 2, 1, 3])) // true
console.log(hasDuplicates([1, 1, 1, 2, 2, 2, 3])) // true
console.log(hasDuplicates([1, 2, 3, 4, 5])) // false





