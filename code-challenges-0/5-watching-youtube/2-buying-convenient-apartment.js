
/*

https://www.youtube.com/watch?v=rw4s4M3hFfs&t=1s&ab_channel=Cl%C3%A9mentMihailescu

*/


/*

let Blocks = [{
    gym: false,
    school: true,
    store: false
}, {
    gym: true,
    school: false,
    store: false
}, {
    gym: true,
    school: true,
    store: false
}, {
    gym: false,
    school: true,
    store: false
}, {
    gym: false,
    school: true,
    store: true
}]

*/
/*

let Blocks = [{
    gym: false, // 1
    school: true, // 0
    store: false // 4
    // max: 4
}, {
    gym: true, // 0
    school: false, // 1
    store: false // 3
    // max: 3
}, {
    gym: true, // 0
    school: true, // 1
    store: false // 2
    // max: 2
}, {
    gym: false, // 1
    school: true, // 0
    store: false // 1
    // max: 1
}, {
    gym: false, // 2
    school: true, // 0
    store: true // 0
    // max: 2
}]

*/

/*

Time Complexity is O(N * K * N)

Space Complexity is O(N)

*/

function findConvenientApartment (list) {
    let itemMap = {}

    let list2 = JSON.parse(JSON.stringify(list))

    for (let itemName in list[0]) {
        if (!list[0].hasOwnProperty(itemName)) {
            continue
        }
        itemMap[itemName] = itemName
    }

    for (let i = 0; i < list.length; i++) { // N - length of list
        let apartment = list[i]
        for (let itemName in apartment) { // K - number of items per list item
            if (apartment[itemName] === true) {
                list2[i][itemName] = 0
            } else if (apartment[itemName] === false) {
                let closestNeighborDistance = Infinity
                for (let j = 0; j < list.length; j++) { // N - length of list
                    let apartment2 = list[j]
                    if (apartment2[itemName] === true) {
                        const distance = Math.abs(i - j)
                        if (distance < closestNeighborDistance) {
                            closestNeighborDistance = distance
                        }
                    }
                }
                list2[i][itemName] = closestNeighborDistance
            }
        }
    }

    let maximumDistanceList = []

    for (let i = 0; i < list2.length; i++) {
        let apartment = list2[i]
        let maximumDistance = -Infinity
        for (let itemName in apartment) {
            if (apartment[itemName] > maximumDistance) {
                maximumDistance = apartment[itemName]
            }
        }
        maximumDistanceList.push(maximumDistance)
    }

    let minimumDistanceApartment = -1
    let minimumDistance = Infinity

    for (let i = 0; i < maximumDistanceList.length; i++) {
        if (maximumDistanceList[i] < minimumDistance) {
            minimumDistanceApartment = i
            minimumDistance = maximumDistanceList[i]
        }
    }

    return minimumDistanceApartment
}



let Blocks = [{
    gym: false, // 1
    school: true, // 0
    store: false // 4
    // max: 4
}, {
    gym: true, // 0
    school: false, // 1
    store: false // 3
    // max: 3
}, {
    gym: true, // 0
    school: true, // 0
    store: false // 2
    // max: 2
}, {
    gym: false, // 1
    school: true, // 0
    store: false // 1
    // max: 1
}, {
    gym: false, // 2
    school: true, // 0
    store: true // 0
    // max: 2
}]


console.log(findConvenientApartment(Blocks))





