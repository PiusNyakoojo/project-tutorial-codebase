

/*

Two players are playing a game of 
Tower Breakers! Player 1 always moves first,
and both players always play optimally. The
rules of the game are as follows.

* Initially there are n towers.
* Each tower is of height m.
* The players move in alternating turns.
* In each turn, a player can choose a tower
    of height x and reduce its height to y,
    where 1 <= y < x and y evenly divides x.
* If the current player is unable to make
    a move, they lose the game.

Given the values of n and m, determine which
player will win. If the first player wins,
return 1. Otherwise, return 2.

Example n = 2
m = 6

There are 2 towers, each 6 units tall.
Player 1 has a choice of two moves:
- remove 3 pieces from a tower to leave
3 as 6 modulo 3 = 0
- remove 5 pieces to leave 1

Let Player 1 remove 3. Now the towers are 3
and 6 units tall.

Player 2 matches the move. Now 

*/

/*
Even though I got the answer and the underlying
logic, I still think this is actually a 
very tough problem. I say so because of the 
following reasons

1. You have to first understand the complicated
game and its rules properly (slightly challenging)

2. You then have to find the correct way to win
this game from the perspective of each player
(extremely challenging)

So basically, there are two cases here. Both
cases rely heavily on the idea that an even
number of towers (say 2n) can be thought of
as a collection of n pairs of towers.

Case 1) If there are an even number of towers
Player 1 will go first, and Player 2 will
basically copy player 1. Whatever player 1
does to a tower, player 2 will do the exact
same thing to the other tower belonging to
the same pair. In this case Player 2 will
always win.

Case 2) If there are an odd number of towers
Player 1 will go first, and simply destroy
any single tower by setting it to 1. Now
we again have the same situation from 
CASE 1 but this time the roles of both players
have been revised. In this case Player 1 will
always win.
Finally, there is also the trivial case where
n does not matter because m = 1. In this case
Player 2 will obviously always win.

*/




function towerBreakers (n, m) {
    if (m == 1) {
        return 2
    } else if (n % 2 == 0) {
        return 2
    } else {
        return 1
    }
}





