

/*
Naive approach
*/

function findNumber (array, k) {
    for (let i = 0; i < array.length; i++) {
        let element = array[i]
        if (element === k) {
            return 'YES'
        }
    }
    return 'NO'
}

console.log(findNumber([1, 2, 3, 10], 10))


/*
Needing to find the length of the array
*/


function findNumber2 (array, k) {
    // find the length of the array
    let foundArrayLength = false
    let arrayLength = 0

    while (foundArrayLength == false) {
        arrayLength++
        if (array[arrayLength] === undefined) {
            foundArrayLength = true
        }
    }

    // find if k is in the array
    for (let i = 0; i < arrayLength; i++) {
        let element = array[i]
        if (element === k) {
            return 'YES'
        }
    }
    return 'NO'
}


console.log(findNumber2([1, 2, 3], 10))


/*
Implemented in Java

https://www.youtube.com/watch?v=HHDtlPOYHTo&ab_channel=Searchingforanswersandknowledge

*/

/*

public static String findNumber(List<Integer> arr, int k) {
    // Write your code here

    // variables
    boolean length = false;
    into lengthTest = 0;
    int arrLength = 0;
    boolean exists = false;

    // find the length of the array
    while (length == false) {
        try {
            lengthTest ++;
            arr.get(lengthTest);
        } catch (Exception ex) {
            arrLength = lengthTest;
            length = true;
        }
    }

    // search the array for the number k
    for (let i = 0; i < arrLength; i++) {
        if (arr.get(i) == k) {
            exists = true;
        }
    }

    // output the result
    if (exists) {
        return 'YES'
    } else {
        return 'NO'
    }
}

*/



