
/*

Julius Caesar protected his confidential
information by encrypting it using a cipher.
Caesar's cipher shifts each letter by a 
number of letters. If the shift takes you
past the end of the alphabet, just rotate
back to the front of the alphabet. In the
case of a rotation 3, w, x, y and z would
map to z, a, b and c.

Original alphabet:
abcdefghijklmnopqrstuvwxyz
Alphabet rotated +3:
defghijklmnopqrstuvwxyzabc

Example
s = There's-a-starman-waiting-in-the-sky
k = 3

The alphabet is rotated by 3, matching
the mapping above. The encrypted string is
Wkhuh'v-d-vwdupdq-zdlwlqj-lq-wkh-vnb

Note: The cipher only encrypts letters;
symbols, such as -, remain unencrypted.

Function description

Complete the caesarCipher function in the
editor below.

caesarCipher has the following parameter(s):

* string s: cleartext
* int k: the alphabet rotation factor

Return

* string: the encrypted string

*/

let alphabet = 'abcdefghijklmnopqrstuvwxyz'

function caesarCipher(s, k) {
    // Write your code here
    let shiftedAlphabet = alphabet.split('').slice(k % alphabet.length).concat(alphabet.substr(0, k % alphabet.length).split('')).join('')
    
    let newString = ''
    
    for (let i = 0; i < s.length; i++) {
        let character = s.charAt(i)
        let isUpperCaseLetter = /[A-Z]/.test(character)
        let indexOfCharacter = alphabet.indexOf(character.toLowerCase())
        let newCharacter = /[A-Za-z]/.test(character) ? shiftedAlphabet.charAt(indexOfCharacter) : character
        newString += isUpperCaseLetter ? newCharacter.toUpperCase() : newCharacter.toLowerCase()
    }
    
    return newString
}


