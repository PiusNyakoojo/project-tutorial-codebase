

/*
Palindrome Index

Given a string of lowercase letters
in the range ascii[a-z], determine
the index of a character that can be
removed to make the string a palindrome.
There may be more than one solution,
but any will do. If the word is already
a palindrome or there is no solution,
return -1. Otherwise, return the index of
a character to remove.

Example
s = 'bcbc'
Either remove 'b' at index 0 or 'c' at
index 3
*/



function palindromeIndex (string) {
    let start = 0
    let end = string.length - 1

    while (start < end && string.charAt(start) === string.charAt(end)) {
        start++
        end--
    }
    // Already a palindrome
    if (start >= end) return -1

    // We need to delete here

    // if it's after the start, that would be a palindrome
    if (isPalindrome(string, start + 1, end)) return start

    // if it's before the end, that's a palindrome
    if (isPalindrome(string, start, end - 1)) return end
    return -1
}



function isPalindrome (string, start, end) {
    while (start < end && string.charAt(start) === string.charAt(end)) {
        start++
        end--
    }
    return start >= end
}




console.log(palindromeIndex('abdcgcba'))




