

/*

A Very Big Sum

https://www.hackerrank.com/challenges/a-very-big-sum/problem?isFullScreen=true

*/

function aVeryBigSum(ar) {
    // Write your code here
    return ar.reduce((a, b) => a + b)
}


