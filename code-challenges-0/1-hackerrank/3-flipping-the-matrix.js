

/*
Sean invented a game involving 2n x 2n
matrix where each cell of the matrix
contains an integer. He can reverse
any of its rows or columns any number
of times. The goal of the game is to maximize
the sum of the elements in the n x n
submatrix located in the upper-left
quadrant of the matrix.
Given the initial configurations of
q matrices, help Sean reverse
the rows and columns of each matrix in
the best possible way so that the sum
of the elements in the matrix's upper-left
quadrant is maximal.
*/

/*

arr[i][j]
arr[i][2 * n - j - 1]
arr[2 * n - i - 1][j]
arr[2 * n - i - 1][2 * n - j - 1]


*/

function findLargestSum (matrix) {
    const n = matrix.length / 2
    let sum = 0

    for (let i = 0; i < matrix.length / 2; i++) {
        for (let j = 0; j < matrix[i].length / 2; j++) {
            // The possibilities for what values
            // can be placed in this position of the
            // matrix by applying a flip operation
            let p1 = matrix[i][j]
            let p2 = matrix[i][2 * n - j - 1]
            let p3 = matrix[2 * n - i - 1][j]
            let p4 = matrix[2 * n - i - 1][2 * n - j - 1]
    
            sum += Math.max(p1, Math.max(p2, Math.max(p3, p4)))
        }
    }

    return sum
}

console.log(findLargestSum([
    [1, 2],
    [3, 4]
]))

console.log(findLargestSum([
    [112, 42, 83, 199],
    [56, 125, 56, 49],
    [15, 78, 101, 43],
    [62, 98, 114, 108]
]))







