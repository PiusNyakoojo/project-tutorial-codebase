
/*

Given an array of n distinct integers,
transform the array into a zig zag sequence
by permuting the array elements. A
sequence will be called a zig zag sequence
if the first k elements in the sequence are
in increasing order and the last k elements
are in decreasing order, where k = (n + 1) / 2.
You need to find the lexicographically smallest
zig zag sequence of the given array.

*/

/*

Lexicographically

In mathematics, the lexicographic or lexicographical
order (also known as lexical order, or dictionary
order) is a generalization of the alphabetical order
of the dictionaries to sequences of ordered symbols or,
more generally, of elements of a totally ordered set.



*/

/*

a = [2, 3, 5, 1, 4]

Now if we permute the array as [1, 4, 5, 3, 2],
the result is a zig zag sequence.

Debug the given function findZigZagSequence to 
return the appropriate zig zag sequence for the
given input array.

Note: You can modify at most three lines in the
given code. You cannot add or remove lines
of code.

To restore the original code, click on the icon
to the right of the language selector.

*/

/*

Consider the sequence 

a = [1, 2, 5, 4, 3]
b = [1, 4, 5, 3, 2]

(1) the two lists differ at index 1
(2) a[1] < b[1] and so 'a' is lexicographically smaller
    than 'b'

*/



/*

Related problem:

Find the lexicographically smallest sequence achievable
https://stackoverflow.com/questions/63635775/find-the-lexicographically-smallest-sequence-achievable

Given a sequence of n integers arr, determine
the lexicographically smallest sequence
which may be obtained from it after performing
at most k element swaps, each involving a
pair of consecutive elements in the sequence.

Note: A list x is lexicographically smaller
than a different equal-length list y if and
only if, for the earliest index at which the
two lists differ, x's element at that index
is smaller than y's element at that index.

For example:

x = [1, 2, 8, 5, 4, 3, 7]
y = [8, 4, 5, 3, 1, 2, 7]

(1) the two lists differ at index 0
(2) x's element x[0] is smaller than y[0]

So x is considered lexicographically smaller
than y.

*/

/*
I'm trying to wrap my head around what the
phrase "lexicographically smaller" implies
based on the above note.

As I understand the English meaning of it,
we are basically talking about a dictionary
order. Let me explain my question with
an example.

Here an example


Example 1
n = 3
k = 2
arr = [5, 3, 1]
output = [1, 5, 3]
We can swap the 2nd and 3rd elements, 
followed by the 1st and 2nd element,
to end up with the sequence [1, 5, 3]

*/

/*

arr = [5, 3, 1], k = 2
[5, 1, 3] // swap [3, 1]
[1, 5, 3] // swap [5, 1]

*/


/*

This is the lexicographically smallest
sequence
achievable after at most 2 swaps.

The above example came with the problem
statement. But wouldn't the lexicographically
smallest sequence instead be
[1, 3, 5] instead of the provided answer
(output) [1, 4, 3]?

Here's another

Example 2
n = 5
k = 3
arr = [8, 9, 11, 2, 1]
output = [2, 8, 9, 11, 1]

We can swap [11, 2], followed by [9, 2],
then [8, 2].

*/

/*
arr = [8, 9, 11, 2, 1]
arr = [8, 9, 2, 11, 1] // swap [11, 2]
arr = [8, 2, 9, 11, 1] // swap [9, 2]
arr = [2, 8, 9, 11, 1] // swap [8, 2]

result:

[2, 8, 9, 11, 1]
*/

/*
Again, the answer I can see in this case is
[1, 2, 8, 11, 9] (after three swaps),
which is the smallest lexicographically and
the provided answer is output [2, 8, 9, 11, 1].

Am I reading the the problem statement incorrectly?
*/

/*

Attempt at solving this problem

*/

function findLexicographicallySmallestSequence(array, k) {
    let sortedArray = [...array].sort((a, b) => a - b)

    let startSwapIndex = 0

    for (let sortedNumber of sortedArray) {
        let indexOfSortedNumber = array.indexOf(sortedNumber)
        if (indexOfSortedNumber <= k) {
            startSwapIndex = indexOfSortedNumber
            break
        }
    }

    for (let i = startSwapIndex; i > 0; i--) {
        let temp = array[i - 1]
        array[i - 1] = array[i]
        array[i] = temp
    }

    return array
}



console.log(findLexicographicallySmallestSequence(
    [5, 3, 1], 2
))

console.log(findLexicographicallySmallestSequence(
    [8, 9, 11, 2, 1], 3
))

