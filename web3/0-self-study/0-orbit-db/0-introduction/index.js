
import * as IPFS from 'ipfs'
import * as OrbitDB from 'orbit-db'

console.log(IPFS)
console.log(OrbitDB)


class OrbitDBDatabaseProvider {

  database

  constructor () {
    this.initializeDatabase()
  }

  async initializeDatabase () {
    const ipfs = await IPFS.create()
    const orbitdb = await OrbitDB.createInstance(ipfs)

    // Create / Open a database
    const database = await orbitdb.docs('orbit.users.shamb0t.profile')
    await database.load()

    this.database = database

    // // Listen for updates from peers
    // database.events.on("replicated", address => {
    //   console.log(database.iterator({ limit: -1 }).collect())
    // })
  }

  async createItem (itemId, item) {
    console.log('create Item')
    // // Add an entry
    // const hash = await this.database.add("world")
    const hash = await this.database.put({ _id: itemId, ...item })
    console.log(hash)
  }

  async getItem (itemId) {
    console.log('get Item')
    // Query
    // const result = this.database.iterator({ limit: -1 }).collect()
    // console.log(JSON.stringify(result, null, 2))
    const profile = await this.database.get(itemId)
    console.log(profile)
  }

  async getItemList () {
    //
    const all = this.database.query((doc) => doc.followers >= 500)
    console.log(all)
  }

  async updateItem () {
    //
  }

  async deleteItem () {
    //
    const hash = await this.database.del('shamb0t')
    console.log(hash)
  }

  async addItemEventListener () {
    //
  }

  async removeItemEventListener () {
    //
  }
}


global.OrbitDBDatabaseProvider = OrbitDBDatabaseProvider


