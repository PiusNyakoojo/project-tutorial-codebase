const path = require("path")

const NodePolyfillPlugin = require('node-polyfill-webpack-plugin')

const webpackConfig = {
  mode: 'development',
  entry: {
    index: path.resolve(__dirname, 'index.js')
  },
  output: {
    filename: 'index.js',
    libraryTarget: 'umd',
    path: path.resolve(__dirname, 'dist')
  },
  plugins: [
    new NodePolyfillPlugin()
  ]
}

module.exports = webpackConfig


