
const { Octokit } = require("@octokit/rest");
const octokit = new Octokit({
  auth: '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'
})


main()


async function main () {
  // createRepository()
  // updateFileContent()
  setGithubPages()
  // pushToRepository('enoplay', 'game-test-0')
}

async function pushToRepository (owner, repo) {
  // (1) Get the last commit sha of a specific
  // branch
  const value = await octokit.rest.repos.getContent({
    owner,
    repo,
  });
  console.log(value)
}

async function updateFileContent () {
  const value = await octokit.rest.repos.createOrUpdateFileContents({
    owner: 'enoplay',
    repo: 'game-test-1',
    path: 'index.html',
    message: 'Add More Text to index.html',
    // sha: 'ad461c157dff0462ccd9ee9ce5fb3f1701600d09',
    content: btoa(`
      <!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Game Test 1 Demo</title>
        <script src="./script/index.js"></script>
      </head>
      <body>
      <h1>Game Test 1</h1>
      </body>
      </html>
    `),
    committer: {
      name:  'Pius Nyakoojo',
      email: 'piusnyakoojo@gmail.com'
    },
    author: {
      name: 'Pius Nyakoojo',
      email: 'piusnyakoojo@gmail.com'
    }
  })
  console.log(value)
}

async function createRepository () {
  const value = await octokit.rest.repos.createInOrg({
    org: 'enoplay',
    name: 'game-test-1',
  });
  console.log(value)
}

async function setGithubPages () {
  const value = await octokit.rest.repos.createPagesSite({
    owner: 'enoplay',
    repo: 'game-test-1',
    source: {
      branch: 'main'
    }
  })
  console.log(value)
}


async function getContent () {
  const value = await octokit.rest.repos.getContent({
    owner: 'enoplay',
    repo: 'game-test-0',
  });
  console.log(value)
  return value
}





