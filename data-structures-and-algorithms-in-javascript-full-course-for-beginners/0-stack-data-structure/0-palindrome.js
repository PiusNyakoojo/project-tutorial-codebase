
/* Stacks! */    
// https://www.youtube.com/watch?v=t2CEgPsws3U&t=1538s&ab_channel=freeCodeCamp.org
// 0:20 - 9:05

// functions: push, pop, peek, length (size)

/*
JavaScript Array object already has all the functions
we need in order to use it as a stack.

We will use an Array stack to find the words that
are palindrom
*/

var letters = []; // this is our stack

var word = 'racecar'

var rword = ''

// put letters of word into stack
for (var i = 0; i < word.length; i++) {
    letters.push(word[i])
}

// pop off the stack in reverse order
for (var i = 0; i < word.length; i++) {
    rword += letters.pop()
}

if (rword === word) {
    console.log(word + ' is a palindrome.')
} else {
    console.log(word + ' is not a palindrome.')
}

