

/*

Database Design. Data Modelling.
Schema Design.

These are all interchangeable words. Terms.

> Ongoing process where:
    - Understand business data
    - Create a logical design (of your database)
        - Tables, Indexes, Constraints (ER Diagram)

Normalization

> Eliminate / Reduce:
    - Data Redundancy
    - Data Anomalies


Multiple occurrences of the same data. Means you
have to update in many places. Not just one place.
Two versions of the data. Can lead to Data 
Anomalies.

*/

/*

Design a database for an ECommerce Website.

(1) Order Number
(2) Device
(3) Product
(4) Product Type
(5) Price
(6) Customer Name
(7) Customer Address
(8) Customer Phone Number
(9) Customer Email
(10) Payment Type
(11) Payment details

De-normalized database.

1 Table, pack all of the information
into that single table of the database.

Start with a de-normalized database table.

Customer Information can be in a separate table.

Orders table

(1) Order number
(2) Device
(3) Product
(4) Product type
(5) Price
(6) Customer ID
(7) Payment Type
(8) Payment details
(9) shipping address
(10) Delivered

..........................................

Process of going from a de-normalized database
to a normalized database.

..........................................

Less Joins, More Storage, High Data Redundancy
[Denormalized Database]

<-------------->

More Joins, Less Storage, Low Data Redundancy
[Normalized Database]

..........................................

Which table to scan first, how should I filter
the data in this table? Result set from this
table and join to another table.

Performance is slower. Many operations happening
at the same time.

..........................................

Data Redundancy is very low. The storage needed
is much less. Normalized database is slow.
Denormalized data is fast. Not always the case.
It depends.

...........................................

*/

/*

We need a Primary Key. Unique Key, Cannot Be NULL

For Example. Order Number. 

Entity-Relationship Design Tool. MySQL Workbench.

ER Diagram. Relationship between two tables.

1 to many relationship.

Each customer can place many orders.

One order can only be done by 1 customer.


Define the datatype of your columns.

varchar. int. text. 

Atomicity. All of the attributes packed into
one column. Address, City, State separately.
Make your database more and more efficient.


Logical Design of an ECommerce Website.




*/






