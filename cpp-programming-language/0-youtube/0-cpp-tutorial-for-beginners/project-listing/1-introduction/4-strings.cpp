


#include <iostream>

using namespace std;

int main () {

    string phrase = "Giraffe Academy";
    // phrase[0] = 'B';
    // cout << phrase.find("Academy", 0) << endl;
    // cout << phrase.substr(8, 3);

    return 0;
}

/*

Learned:

- cout << phrase[0];
- phrase[0] = 'B'
- phrase.find("Academy", 0)
- phrase.substr(8, 3)

*/

