
#include <iostream>

using namespace std;

int main () {
    cout << "Hello World!" << endl;
    cout << "My name is Pius Nyakoojo" << endl;

    cout << "     /|" << endl;
    cout << "    / |" << endl;
    cout << "   /  |" << endl;
    cout << "  /   |" << endl;
    cout << " /____|" << endl;

    cout << " /____|" << endl;
    cout << "     /|" << endl;
    cout << "    / |" << endl;
    cout << "   /  |" << endl;
    cout << "  /   |" << endl;

    return 0;
}

/*

cout. endl.

*/

