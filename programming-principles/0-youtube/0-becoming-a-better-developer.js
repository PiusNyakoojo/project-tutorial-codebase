

/*

Becoming a better developer by using the SOLID design principles by Katerina Trajchevska
By Laracon EU
https://www.youtube.com/watch?v=rtmFCcjEgEw&ab_channel=LaraconEU

Working on a startup product

- In charge of the development porcess
- Constantly adding new features
- No formal process
- Very dynamic environment, no time to worry about code structure
- What is it like to go back to your code after 2 years?

The purpose of SOLID design principles

- To make the code more maintainable
- To make it easier to quickly extend the system with new
functionality without breaking the existing ones.
- o make the code easier to read and understand,
thus spend less time figuring out what it does
and more time acutlaly developing the solution
- Introduced by Robert Martin (Uncle Bob), named
by Michael Feathers.

Single Responsibility Principle.
- A class should have one, and only one, reason
to change.


Open / Closed Principle
- An entity should be open for extension but 
closed for modification.
- Extend functionality by adding new code instead
of changing existing code.
- Separate the behaviors, so the system can easily
be extended, but never broken.
- Goal: get to a poin where you can never break
the core of your system.


Liskov Substitution Principles
Le O(x) be a property provable about objects x
of type T. Then O(y) should be rue for objects
y of type S where S is a subype of T.

Liskov Substitution Principles
- Any derived class should be able to substitute
its parent class without the consumer knowing it.
- Every class that implements an interface, mus
be able to substitute any reference throughou
the code that implements that same interface.
- Every part of the code should get the expected
result no matter what instance of a class you
send to it, given it implements the same interface.


Inerface Segregation Principles
No client should be forced to depend on methods
it does not use.

- A client should never be forced to depend on
methods it doesn't use.
- Or, a client should never depend on anything
more than the method it's calling.
- Changing one method in a class shouldn't
affect classes that don't depend on it.
- Replace fat interfaces wih many small,
specific interfaces.

Dependency Inversion Principle
High-level modules should not depend on low-level
modules. Both should depend on abstractions.

- Never depend on anything concrete, only depend
on abstractions.
- High level modules should not depend on low level
modules. They should depend on abstractions.
- Able to change an implementation easily
without alering the high level code.

Don't get trapped by SOLID
- SOLID design principles are principles, not rules.
- Always use common sense when applying SOLID.
- Avoid over-fragmenting your code for the sake
of SRP or SOLID.
- Don't try to achieve SOLID, use SOLID to 
achieve maintainability.


*/




